package danger.warning.system.beans;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDateTime;
import danger.warning.system.dto.Category;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.PrimeFaces;

import danger.warning.system.dao.CallForHelpDAO;
import danger.warning.system.dto.CallForHelp;


@ManagedBean(name= "callForHelpBean")
@RequestScoped
public class CallForHelpBean implements Serializable{

	private static final long serialVersionUID = 4075449356721952957L;
	
	private String title;
	private String location;
	private LocalDateTime dateTime;
	private String description;
	private String imageUri;
	private String category;
	
	public CallForHelpBean() {
	}
	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String submit() {
		
		CallForHelp callForHelp = new CallForHelp(null, this.getTitle(), this.getLocation(), this.getDateTime(), this.getDescription(), URI.create(this.getImageUri()), Category.valueOf(this.getCategory()), false, false);
		
		if(! new CallForHelpDAO().addCallForHelp(callForHelp)) 
			PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage("There was Erorr while trying to add new call for help"));
		this.title = "";
		this.location = "";
		this.dateTime = null;
		this.imageUri = "";
		this.description = "";
		return "index.xhtml?faces-redirect=true";
	}
}
