package danger.warning.system.rss;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndCategoryImpl;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedOutput;

import danger.warning.system.dao.CallForHelpDAO;

@WebServlet(name = "RSSServlet", urlPatterns = {"/rss"})
public class RSSServlet extends HttpServlet {

	private static final long serialVersionUID = -7918326448631377246L;

	public RSSServlet() {
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SyndFeed feed = new SyndFeedImpl();

		feed.setFeedType("rss_2.0");
		feed.setTitle("Danger Warning System - Calls For Help");
		feed.setDescription("All calls for help");
		feed.setLink("https://gitlab.com/Vrljanovic");
		List<SyndEntry> entries = new LinkedList<>();
		
			new CallForHelpDAO().getAllCalls().parallelStream().filter(c -> !c.isBlocked()).forEach(c -> {
				SyndEntryImpl entry = new SyndEntryImpl();
				entry.setTitle(c.getTitle());
				SyndContentImpl content = new SyndContentImpl();
				content.setValue(c.getDescription());
				entry.setDescription(content);
				SyndCategory category = new SyndCategoryImpl();
				category.setName(c.getCategory().getCategory());
				LinkedList<SyndCategory> categories = new LinkedList<>();
				categories.add(category);
				entry.setCategories(categories);
				entries.add(entry);
			});
			
			feed.setEntries(entries);
	        try {
	            response.getWriter().println(new SyndFeedOutput().outputString(feed));
	        } catch (FeedException ex) {
	            response.sendError(500);
	        }

		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
