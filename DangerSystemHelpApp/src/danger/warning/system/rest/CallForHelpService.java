package danger.warning.system.rest;

import java.net.HttpURLConnection;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import danger.warning.system.dao.CallForHelpDAO;
import danger.warning.system.dto.CallForHelp;

@Path("/help")
public class CallForHelpService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response method() {
		return Response.ok().entity(new CallForHelpDAO().getAllCalls()).build();
	}
	
	@POST
	@Path("/block")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response blockUnblock(CallForHelp call) {
		if (call == null)
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).build();
		call.setBlocked(! call.isBlocked());
		if (new CallForHelpDAO().blockUnblockCall(call))
			return Response.ok().entity("OK").build();
		return Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).build();
	}
	
	@POST
	@Path("/report")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response report(CallForHelp call) {
		if (call == null)
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).build();
		call.setReported(true);
		if (new CallForHelpDAO().reportCall(call))
			return Response.ok().entity("OK").build();
		return Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).build();
	}
}
