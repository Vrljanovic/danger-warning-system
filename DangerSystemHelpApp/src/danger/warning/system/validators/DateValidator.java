package danger.warning.system.validators;

import java.time.LocalDateTime;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.PrimeFaces;

@FacesValidator("danger.warning.system.validators.DateValidator")
public class DateValidator implements Validator {
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		
		LocalDateTime dateTime = (LocalDateTime)arg2;
		
		if(dateTime.isBefore(LocalDateTime.now()) || dateTime.isEqual(LocalDateTime.now())){
			PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid date!", "Date must be greater then now."));
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid date!", "Date must be greater then now."));
		}
		
	}

}
