package danger.warning.system.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.commons.validator.routines.UrlValidator;
import org.primefaces.PrimeFaces;

@FacesValidator("danger.warning.system.validators.URIValidator")
public class URIValidator implements Validator {
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		String[] schemes = { "http", "https" };
		UrlValidator urlValidator = new UrlValidator(schemes);
		if (!urlValidator.isValid(arg2.toString())) {
			PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid url!", "Url of image is not valid!"));
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid url!", "Url of image is not valid!"));
		}	
	}
}
