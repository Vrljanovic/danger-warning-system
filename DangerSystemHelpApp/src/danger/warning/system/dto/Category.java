package danger.warning.system.dto;

public enum Category {
    
    DAMAGE_REPAIRING_ASSISTENACE("DAMAGE_REPAIRING_ASSISTENACE"),
    COLLECTING_CLOTHES("COLLECTING_CLOTHES"),
    COLLECTING_FOODSTUFFS("COLLECTING_FOODSTUFFS"),
    TRANSPORT("TRANSPORT"),
    OTHER("OTHER");
    
    private String category;
    
    Category(String category) {
        this.category = category;
    }
 
    public String getCategory() {
        return this.category;
    }

}
