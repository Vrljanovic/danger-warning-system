const BATTUTA_API_KEY = "4827a079eac56523b98ad0cfd3a217b4";
var flags = [];

history.replaceState("", "", '/');

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		var countrySelect = document.getElementById("country");
		var countriesHTML = `<option value=0 disabled selected>Select</option>`;
		Object.values(JSON.parse(this.responseText)).forEach(element => {
			countriesHTML += `<option value=${element.alpha2Code}>${element.name}</option>`;
			flags.push(element.flag);
		});
		countrySelect.innerHTML = countriesHTML;
	}
};
xhttp.open("GET", "https://restcountries.eu/rest/v2/region/europe", true);
xhttp.send();

addRegions = () => {
	var alpha2Code = document.getElementById("country").value;
	var img = document.getElementById("avatar");
	img.src = flags[document.getElementById("country").selectedIndex - 1];
	document.getElementById("file-input").value = null;
	if (alpha2Code == 0)
		return;
	var url = "http://battuta.medunes.net/api/region/" + alpha2Code + "/all/?key=" + BATTUTA_API_KEY + "&callback=cb";
	JsonHttpRequest(url, "cb", "region");
	
}

addCities = () => {
	var alpha2Code = document.getElementById("country").value;
	var region = document.getElementById("region").value;
	if (alpha2Code == 0 || region == 0)
		return;
	var url = "http://battuta.medunes.net/api/city/" + alpha2Code + "/search/?region=" + region + "&key=" + BATTUTA_API_KEY + "&callback=cb";
	JsonHttpRequest(url, "cb", "city");
}

function JsonHttpRequest(url, callback, selectName) {
	var e = document.createElement('script');
	e.src = url;
	document.head.appendChild(e);

	window[callback] = (data) => {
		var html = `<option value=0 disabled selected>Select</option>`;
		var select = null;
		if (selectName == "region") {
			select = document.getElementById("region");
			data.forEach(element => {
				html += `<option value="${element.region}">${element.region}</option>`;
			})
			changeAvatarValue();
		}
		else {
			select = document.getElementById("city");
			data.forEach(element => {
				html += `<option value="${element.city}">${element.city}</option>`;
			})
		}
		select.innerHTML = html;
	}
}

chooseAvatar = () => {
	var input = document.createElement("input");
	input.type = "file";
	input.trigger("click");
	return;
}

loadFile = (event) => {
	var output = document.getElementById('avatar');
	output.src = URL.createObjectURL(event.target.files[0]);
	output.onload = function() {
		URL.revokeObjectURL(output.src) 
		var el = document.getElementById("avatar-val");
		el.value = null;
	}
};

changeAvatarValue = () => {
	var el = document.getElementById("avatar-val");
	el.value = document.getElementById("avatar").src;
}