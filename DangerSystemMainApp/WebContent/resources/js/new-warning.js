function initMap() {
	var bl = { lat: 44.772182, lng: 17.191000 };
	var map = new google.maps.Map(document.getElementById('map'), { zoom: 16, center: bl });
	var marker = new google.maps.Marker({ position: bl, draggable:true });
	google.maps.event.addListener(marker, 'dragend', function() { 
		var latitude = document.getElementById("latitude");
		latitude.value = marker.position.lat();
		var longitude = document.getElementById("longitude");
		longitude.value = marker.position.lng();
	 } );
	marker.setMap(map);
}

history.replaceState("", "", '/');