const YOUTUBE_URL_REGEX = new RegExp("(https?://)?((www\.)?youtube\.com|youtu\.?be)/([^ \n]+)+");
const YOUTU_BE_REGEX = new RegExp("(https?:\/\/)?((www\.)?youtu\.?be)\/([^ \n]+)+");

history.replaceState("", "", '/');

showPost = (id, userName, userAvatar) => {
	var postsHTML = "";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var postsDiv = document.getElementById("post-part");
			var temp_post = JSON.parse(this.responseText);
			var post = document.createElement("div");
			post.classList.add("single-post")
			post.id = "post-div-" + temp_post.id;

			var postHeader = document.createElement("div");
			postHeader.classList.add("single-post-header");
			//Add avatar 
			var avatarDiv = document.createElement("div");
			avatarDiv.classList.add("single-post-avatar-div");
			var avatar = document.createElement("img");
			avatar.classList.add("single-post-avatar");
			avatar.src = temp_post.user.avatar;
			avatarDiv.appendChild(avatar);
			postHeader.appendChild(avatarDiv);

			//Add user's name
			var userNameLabel = document.createElement("label");
			userNameLabel.classList.add("single-post-name-label");
			userNameLabel.innerHTML = temp_post.user.name + " " + temp_post.user.surname;
			postHeader.appendChild(userNameLabel);

			//Add posted at datetime
			var dateTimeLink = document.createElement("a");
			dateTimeLink.classList.add("post-datetime-link");
			dateTimeLink.classList.add("float-md-right");
			dateTimeLink.href = "/post?post_id=" + temp_post.id;
			var dateTime = temp_post.postedAt;
			time = dateTime.date.day + "." + dateTime.date.month + "." + dateTime.date.year + ". ";
			time += dateTime.time.hour + ":" + dateTime.time.minute + ":" + dateTime.time.second;
			dateTimeLink.innerText = time;
			postHeader.appendChild(dateTimeLink);
			post.appendChild(postHeader);

			//post.appendChild(document.createElement("hr"));

			//Add text
			var textDiv = document.createElement("div");
			var textP = document.createElement("p");
			textP.classList.add("single-post-content");
			textP.innerText = temp_post.text;
			textDiv.appendChild(textP);
			post.appendChild(textDiv);

			var attachmentDiv = document.createElement("div");
			attachmentDiv.classList.add("post-attachments");
			if (temp_post.postType == "YOUTUBE_VIDEO") {
				var result = YOUTUBE_URL_REGEX.exec(temp_post.text);
				var iframe = document.createElement("iframe");
				iframe.classList.add("youtube-post")
				iframe.src = "https://www.youtube.com/embed/" + getYoutubeVideoID(result[0]);
				attachmentDiv.appendChild(iframe);
				textP.innerText = textP.innerText.replace(result[0], "");
			} else if (temp_post.postType == "VIDEO") {
				var video = document.createElement("video");
				var source = document.createElement("source");
				video.classList.add("video-post");
				source.src = temp_post.attachments[0].path;
				video.controls = true;
				video.autoplay = false;
				video.appendChild(source);
				attachmentDiv.appendChild(video);
				video.load();
			} else if (temp_post.postType == "IMAGE") {
				var mainDiv = document.createElement("div");
				mainDiv.classList.add("carousel");
				mainDiv.classList.add("slide");
				mainDiv.setAttribute("data-ride", "carousel");
				mainDiv.id = "post-images-" + temp_post.id;

				var innerDiv = document.createElement("div");
				innerDiv.classList.add("carousel-inner");

				for (i = 0; i < temp_post.attachments.length; ++i) {
					var itemDiv = document.createElement("div");
					itemDiv.classList.add("carousel-item");
					if (i == 0)
						itemDiv.classList.add('active');
					var img = document.createElement("img");
					var div = document.createElement("div");
					div.classList.add("single-user-post-image-div");
					img.classList.add("d-block");
					img.classList.add("single-user-post-image");
					img.classList.add("w-100");
					img.src = temp_post.attachments[i].path;
					div.appendChild(img)
					itemDiv.appendChild(div);
					innerDiv.appendChild(itemDiv);
				}
				attachmentDiv.appendChild(innerDiv);

				//create controlls

				var left = document.createElement("a");
				left.classList.add("carousel-control-prev");
				left.href = "#post-images-" + temp_post.id;
				left.setAttribute("data-slide", "prev");
				left.setAttribute("role", "button");

				var leftSpan1 = document.createElement("span");
				leftSpan1.classList.add("carousel-control-prev-icon");
				leftSpan1.setAttribute("aria-hidden", "true");

				left.appendChild(leftSpan1);

				var leftSpan2 = document.createElement("span");
				leftSpan2.classList.add("sr-only");
				leftSpan2.innerText = "Previous";

				left.appendChild(leftSpan2);

				var right = document.createElement("a");
				right.classList.add("carousel-control-next");
				right.href = "#post-images-" + temp_post.id;
				right.setAttribute("data-slide", "next");
				right.setAttribute("role", "button");

				var rightSpan1 = document.createElement("span");
				rightSpan1.classList.add("carousel-control-next-icon");
				rightSpan1.setAttribute("aria-hidden", "true");

				right.appendChild(rightSpan1);

				var rightSpan2 = document.createElement("span");
				rightSpan2.classList.add("sr-only");
				rightSpan2.innerText = "Previous";

				right.appendChild(rightSpan2);

				mainDiv.appendChild(innerDiv);
				mainDiv.appendChild(left);
				mainDiv.appendChild(right);
				attachmentDiv.appendChild(mainDiv);

			}

			post.appendChild(attachmentDiv);
			var urlRegex = /(https?:\/\/[^\s]+)/g;
			textP.innerHTML = textP.innerText.replace(urlRegex, function(url) {
				return '<a href="' + url + '" target="_blank">' + url + '</a>';
			});

			post.appendChild(document.createElement("hr"));


			//Add post footer

			var postFooter = document.createElement("div");
			postFooter.classList.add("single-post-footer-div");

			var a = document.createElement("a");
			a.innerText = "Comments";
			a.id = "add-comment-button-" + temp_post.id;
			a.classList.add("add-comment-button");
			a.href = "javascript: showOrHideComments(" + temp_post.id + ", '" + userName + "', '" + userAvatar + "')";
			a.classList.add("btn");
			a.classList.add("btn-primary");

			postFooter.appendChild(a);

			var linkDiv = document.createElement("div");
			linkDiv.classList.add("share-link-div");
			linkDiv.classList.add("float-md-right");
			var facebookLink = document.createElement("a");
			facebookLink.classList.add("share-link-label");
			facebookLink.href = "https://www.facebook.com/sharer/sharer.php?u=http://192.168.100.5:8080/post?post_id=" + temp_post.id;
			facebookLink.innerText = "Facebook";
			facebookLink.target = "_blank";
			linkDiv.appendChild(facebookLink);

			var twitterLink = document.createElement("a");
			twitterLink.href = "https://twitter.com/home?status=http://192.168.100.5:8080/post?post_id=" + temp_post.id;
			twitterLink.innerText = "Twitter";
			twitterLink.target = "_blank";
			twitterLink.classList.add("share-link-label");
			linkDiv.appendChild(twitterLink);
			postFooter.appendChild(linkDiv);

			post.appendChild(postFooter);

			postsHTML += post.outerHTML;

			postsDiv.innerHTML += postsHTML;
		}
	};
	xhttp.open("GET", "http://localhost:8080/user-posts?post_id=" + id, true);
	xhttp.send();
}

showOrHideComments = (id, userName, userAvatar) => {
	var postDiv = document.getElementById("post-div-" + id);
	if (postDiv.lastChild.id == "comment-section-div-" + id)
		postDiv.removeChild(document.getElementById("comment-section-div-" + id));
	else {
		var commentSectionDiv = document.createElement("div");
		commentSectionDiv.id = "comment-section-div-" + id;
		commentSectionDiv.classList.add("comment-section-div");
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var post = JSON.parse(this.responseText)
				for (i = 0; i < post.comments.length; ++i) {
					var commentDiv = document.createElement("div");

					var commentHeader = document.createElement("div");

					var avatar = document.createElement("img");
					avatar.src = post.comments[i].user.avatar;
					avatar.classList.add("comment-user-avatar");
					commentHeader.appendChild(avatar);

					var nameLabel = document.createElement("label");
					nameLabel.classList.add("comment-name-label");
					nameLabel.innerText = post.comments[i].user.name + " " + post.comments[i].user.surname;
					commentHeader.appendChild(nameLabel);

					var commentedAtLabel = document.createElement("label");
					commentedAtLabel.classList.add("commented-at-label");
					commentedAtLabel.classList.add("float-md-right");
					var dateTime = post.comments[i].commentedAt;
					var time = dateTime.date.day + "." + dateTime.date.month + "." + dateTime.date.year + ". ";
					time += dateTime.time.hour + ":" + dateTime.time.minute + ":" + dateTime.time.second;
					commentedAtLabel.innerText = time;
					commentHeader.appendChild(commentedAtLabel);

					var comment = document.createElement("p");
					comment.classList.add("comment-text");
					comment.innerText = post.comments[i].text;

					commentDiv.appendChild(commentHeader);
					commentDiv.appendChild(comment);

					if (post.comments[i].imagePath != undefined) {
						var commentImg = document.createElement("img");
						commentImg.src = post.comments[i].imagePath;
						commentImg.classList.add("comment-img");
						commentDiv.append(commentImg);
					}
					commentDiv.appendChild(document.createElement("hr"));

					commentSectionDiv.appendChild(commentDiv);
				}
			}
		};
		xhttp.open("GET", "http://localhost:8080/user-posts?post_id=" + id, false);
		xhttp.send();

		var addCommentDiv = document.createElement("div");
		var form = document.createElement("form");
		form.enctype = "multipart/form-data";
		var img = document.createElement("img");
		if (userAvatar.indexOf("http") != -1)
			img.src = userAvatar;
		else
			img.src = "user_avatars\\" + userAvatar;
		img.classList.add("add-comment-avatar");
		addCommentDiv.appendChild(img);

		var label = document.createElement("label");
		label.classList.add("add-comment-label");
		label.innerText = userName;

		addCommentDiv.appendChild(label);
		var action = document.createElement("input");
		action.type = "hidden";
		action.name = "action";
		action.value = "add-comment";
		form.append(action);

		var postIdField = document.createElement("input");
		postIdField.type = "hidden";
		postIdField.name = "post_id";
		postIdField.value = id;
		form.append(postIdField);

		var commentTextField = document.createElement("input");
		commentTextField.classList.add("form-control");
		commentTextField.name = "text";
		commentTextField.type = "text";
		commentTextField.placeholder = "Add your comment here";

		addCommentDiv.appendChild(commentTextField);

		form.append(commentTextField);

		var chooseImageLabel = document.createElement("label");
		chooseImageLabel.htmlFor = "comment-image";
		chooseImageLabel.innerText = "Choose image\t";
		chooseImageLabel.htmlFor = "comment-image";
		form.appendChild(chooseImageLabel);

		var chooseImage = document.createElement("input");
		chooseImage.type = "file";
		chooseImage.name = "comment-image";
		chooseImage.id = "comment-image";
		chooseImage.accept = "image/*";

		form.appendChild(chooseImage);

		var addButton = document.createElement("a");
		addButton.innerText = "Add comment";
		addButton.classList.add("btn");
		addButton.classList.add("add-comment-button")
		addButton.classList.add("btn-info");
		addButton.classList.add("float-md-right");
		form.appendChild(addButton);
		addButton.onclick = () => {
			var formData = new FormData(form);
			xhttp = new XMLHttpRequest();
			xhttp.open("POST", "http://localhost:8080/add-comment", false);
			xhttp.send(formData);

			var btnComments = document.getElementById("add-comment-button-" + id);
			btnComments.click();
			showOrHideComments(id);
		}
		addCommentDiv.appendChild(form);
		commentTextField.onkeydown = (event) => {
			if (event.key === "Enter")
				addButton.click();
		}


		commentSectionDiv.appendChild(addCommentDiv);
		postDiv.appendChild(commentSectionDiv);
	}
}

getYoutubeVideoID = (url) => {
	var youtu_be = YOUTU_BE_REGEX.exec(url);
	var video_id = "";
	if (youtu_be != null) {
		var index__of_slash = youtu_be[0].lastIndexOf("/");
		video_id = youtu_be[0].substring(index__of_slash + 1, youtu_be[0].length);
		var spacePosition = video_id.indexOf(" ");
		if (spacePosition != -1)
			video_id = video_id.substring(0, spacePosition);
	} else {
		video_id = url.split('v=')[1];
		var ampersandPosition = video_id.indexOf('&');
		var spacePosition = video_id.indexOf(" ");
		if (ampersandPosition != -1) {
			video_id = video_id.substring(0, ampersandPosition);
		}
		if (spacePosition != -1 && ampersandPosition > spacePosition)
			video_id = video_id.substring(0, spacePosition);
	}
	return video_id;
}