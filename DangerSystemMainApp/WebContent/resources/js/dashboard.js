const BATTUTA_API_KEY = "4827a079eac56523b98ad0cfd3a217b4";
const WEATHER_API_KEY = "8ec51647a6a6e72930e04820fee8daab";
const YOUTUBE_URL_REGEX = new RegExp("(https?://)?((www\.)?youtube\.com|youtu\.?be)/([^ \n]+)+");
const YOUTU_BE_REGEX = new RegExp("(https?:\/\/)?((www\.)?youtu\.?be)\/([^ \n]+)+");

if (window.history.replaceState) {
	window.history.replaceState(null, null, window.location.href);
}

document.onkeydown = (event) => {
	if (event.key === "Escape") {
		var modal = document.getElementById("myModal");
		if (modal.style.display != "None")
			modal.style.display = "None";
	}
}

history.replaceState("", "", '/');

init = (country, region, homeCity) => {
	getPosts();
	getNotifications();
	getWeather(country, region, homeCity);
	setInterval(getPosts, 60000);
	setInterval(getNotifications, 10000);
}



getWeather = (country, region, homeCity) => {
	var cities = [];
	var callback = "cb";
	var url = "http://battuta.medunes.net/api/city/" + country + "/search/?region=" + region + "&key=" + BATTUTA_API_KEY + "&callback=cb";
	var e = document.createElement('script');
	e.src = url;
	document.head.appendChild(e);

	window[callback] = (data) => {
		data.forEach(element => {
			let city = element.city;
			if (city != homeCity) {
				if (city.indexOf("Opstina") != -1)
					city = city.replace("Opstina ", "");
				else if (city.indexOf("Opcina" != -1))
					city = city.replace("Opcina ", "");
				cities.push(city);
			}
		});

		if (homeCity.indexOf("Opstina") != -1)
			homeCity = homeCity.replace("Opstina ", "")
		else if (homeCity.indexOf("Opcina" != -1))
			homeCity = homeCity.replace("Opcina ", "");
		getWeatherForCity(homeCity);
		var pos = Math.floor(Math.random() * cities.length);
		getWeatherForCity(cities[pos]);
		var pos2 = Math.floor(Math.random() * cities.length);
		getWeatherForCity(cities[pos2]);
	}
}
getWeatherForCity = (city) => {

	var callback = "cc";
	var weatherUrl = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + WEATHER_API_KEY + "&callback=cc&units=metric";

	var e = document.createElement("script");
	e.src = weatherUrl;
	document.head.appendChild(e);
	window[callback] = (data) => {
		var rightSide = document.getElementById("right");
		var weatherElement = document.createElement("div");
		weatherElement.classList.add("city-weather");
		var info = document.createElement("p");
		info.classList.add("weather-text");
		info.innerHTML = data.name + " " + data.main.temp + " &#8451;";
		var icon = document.createElement("img");
		icon.classList.add("weather-icon");
		icon.src = "http://openweathermap.org/img/wn/" + data.weather[0].icon + ".png"
		weatherElement.appendChild(info);
		weatherElement.appendChild(icon);
		rightSide.appendChild(weatherElement);

	}
}

getPosts = () => {
	var postsDiv = document.getElementById("posts");
	postsDiv.innerHTML = "";
	getUserPosts();
	getRSSPosts();
}

getUserPosts = () => {
	var postsHTML = "";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var postsDiv = document.getElementById("posts");
			Object.values(JSON.parse(this.responseText)).forEach(post => {
				var temp_post = post;
				var post = document.createElement("div");
				post.classList.add("post")
				post.id = "post-div-" + temp_post.id;

				var postHeader = document.createElement("div");
				postHeader.classList.add("post-header");
				//Add avatar 
				var avatarDiv = document.createElement("div");
				avatarDiv.classList.add("post-avatar-div");
				var avatar = document.createElement("img");
				avatar.classList.add("post-avatar");
				avatar.src = temp_post.user.avatar;
				avatarDiv.appendChild(avatar);
				postHeader.appendChild(avatarDiv);

				//Add user's name
				var userNameLabel = document.createElement("label");
				userNameLabel.classList.add("name-label");
				userNameLabel.innerHTML = temp_post.user.name + " " + temp_post.user.surname;
				postHeader.appendChild(userNameLabel);

				//Add posted at datetime
				var dateTimeLink = document.createElement("a");
				dateTimeLink.classList.add("post-datetime-link");
				dateTimeLink.classList.add("float-md-right");
				dateTimeLink.href = "/post?post_id=" + temp_post.id;
				var dateTime = temp_post.postedAt;
				time = dateTime.date.day + "." + dateTime.date.month + "." + dateTime.date.year + ". ";
				time += dateTime.time.hour + ":" + dateTime.time.minute + ":" + dateTime.time.second;
				dateTimeLink.innerText = time;
				postHeader.appendChild(dateTimeLink);

				post.appendChild(postHeader);

				post.appendChild(document.createElement("hr"));

				//Add text
				var textDiv = document.createElement("div");
				var textP = document.createElement("p");
				textP.classList.add("post-content");
				textP.innerText = temp_post.text;
				textDiv.appendChild(textP);
				post.appendChild(textDiv);

				var attachmentDiv = document.createElement("div");
				attachmentDiv.classList.add("post-attachments");
				if (temp_post.postType == "YOUTUBE_VIDEO") {
					var result = YOUTUBE_URL_REGEX.exec(temp_post.text);
					var iframe = document.createElement("iframe");
					iframe.classList.add("youtube-post")
					iframe.src = "https://www.youtube.com/embed/" + getYoutubeVideoID(result[0]);
					attachmentDiv.appendChild(iframe);
					textP.innerText = textP.innerText.replace(result[0], "");
				} else if (temp_post.postType == "VIDEO") {
					var video = document.createElement("video");
					var source = document.createElement("source");
					video.classList.add("video-post");
					source.src = temp_post.attachments[0].path;
					video.controls = true;
					video.autoplay = false;
					video.appendChild(source);
					attachmentDiv.appendChild(video);
					video.load();
				} else if (temp_post.postType == "IMAGE") {
					var mainDiv = document.createElement("div");
					mainDiv.classList.add("carousel");
					mainDiv.classList.add("slide");
					mainDiv.setAttribute("data-ride", "carousel");
					mainDiv.id = "post-images-" + temp_post.id;

					var innerDiv = document.createElement("div");
					innerDiv.classList.add("carousel-inner");

					for (i = 0; i < temp_post.attachments.length; ++i) {
						var itemDiv = document.createElement("div");
						itemDiv.classList.add("carousel-item");
						if (i == 0)
							itemDiv.classList.add('active');
						var img = document.createElement("img");
						var div = document.createElement("div");
						div.classList.add("user-post-image-div");
						img.classList.add("d-block");
						img.classList.add("user-post-image");
						img.classList.add("w-100");
						img.src = temp_post.attachments[i].path;
						div.appendChild(img)
						itemDiv.appendChild(div);
						innerDiv.appendChild(itemDiv);
					}
					attachmentDiv.appendChild(innerDiv);

					//create controlis

					var left = document.createElement("a");
					left.classList.add("carousel-control-prev");
					left.href = "#post-images-" + temp_post.id;
					left.setAttribute("data-slide", "prev");
					left.setAttribute("role", "button");

					var leftSpan1 = document.createElement("span");
					leftSpan1.classList.add("carousel-control-prev-icon");
					leftSpan1.setAttribute("aria-hidden", "true");

					left.appendChild(leftSpan1);

					var leftSpan2 = document.createElement("span");
					leftSpan2.classList.add("sr-only");
					leftSpan2.innerText = "Previous";

					left.appendChild(leftSpan2);

					var right = document.createElement("a");
					right.classList.add("carousel-control-next");
					right.href = "#post-images-" + temp_post.id;
					right.setAttribute("data-slide", "next");
					right.setAttribute("role", "button");

					var rightSpan1 = document.createElement("span");
					rightSpan1.classList.add("carousel-control-next-icon");
					rightSpan1.setAttribute("aria-hidden", "true");

					right.appendChild(rightSpan1);

					var rightSpan2 = document.createElement("span");
					rightSpan2.classList.add("sr-only");
					rightSpan2.innerText = "Previous";

					right.appendChild(rightSpan2);

					mainDiv.appendChild(innerDiv);
					mainDiv.appendChild(left);
					mainDiv.appendChild(right);
					attachmentDiv.appendChild(mainDiv);

				}

				post.appendChild(attachmentDiv);
				var urlRegex = /(https?:\/\/[^\s]+)/g;
				textP.innerHTML = textP.innerText.replace(urlRegex, function(url) {
					return '<a href="' + url + '" target="_blank">' + url + '</a>';
				});

				post.appendChild(document.createElement("hr"));

				var postFooter = document.createElement("div");
				postFooter.classList.add("post-footer-div");

				//Add post footer

				var postFooter = document.createElement("div");
				postFooter.classList.add("post-footer-div");

				var a = document.createElement("a");
				a.innerText = "Comments";
				a.id = "add-comment-button-" + temp_post.id;
				a.classList.add("add-comment-button");
				a.href = "javascript: showOrHideComments(" + temp_post.id + ")";
				a.classList.add("btn");
				a.classList.add("btn-primary");

				postFooter.appendChild(a);

				var linkDiv = document.createElement("div");
				linkDiv.classList.add("share-link-div");
				linkDiv.classList.add("float-md-right");
				var facebookLink = document.createElement("a");
				facebookLink.classList.add("share-link-label");
				facebookLink.href = "https://www.facebook.com/sharer/sharer.php?u=http://192.168.100.5:8080/post?post_id=" + temp_post.id;
				facebookLink.innerText = "Facebook";
				facebookLink.target = "_blank";
				linkDiv.appendChild(facebookLink);

				var twitterLink = document.createElement("a");
				twitterLink.href = "https://twitter.com/home?status=http://192.168.100.5:8080/post?post_id=" + temp_post.id;
				twitterLink.innerText = "Twitter";
				twitterLink.target = "_blank";
				twitterLink.classList.add("share-link-label");
				linkDiv.appendChild(twitterLink);
				postFooter.appendChild(linkDiv);

				post.appendChild(postFooter);

				postsHTML += post.outerHTML;
			});

			postsDiv.innerHTML += postsHTML;
		}
	};
	xhttp.open("GET", "http://localhost:8080/user-posts", true);
	xhttp.send();
}

showOrHideComments = (id) => {
	var postDiv = document.getElementById("post-div-" + id);
	if (postDiv.lastChild.id == "comment-section-div-" + id)
		postDiv.removeChild(document.getElementById("comment-section-div-" + id));
	else {
		var commentSectionDiv = document.createElement("div");
		commentSectionDiv.id = "comment-section-div-" + id;
		commentSectionDiv.classList.add("comment-section-div");
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var post = JSON.parse(this.responseText)
				for (i = 0; i < post.comments.length; ++i) {
					var commentDiv = document.createElement("div");

					var commentHeader = document.createElement("div");

					var avatar = document.createElement("img");
					avatar.src = post.comments[i].user.avatar;
					avatar.classList.add("comment-user-avatar");
					commentHeader.appendChild(avatar);

					var nameLabel = document.createElement("label");
					nameLabel.classList.add("comment-name-label");
					nameLabel.innerText = post.comments[i].user.name + " " + post.comments[i].user.surname;
					commentHeader.appendChild(nameLabel);

					var commentedAtLabel = document.createElement("label");
					commentedAtLabel.classList.add("commented-at-label");
					commentedAtLabel.classList.add("float-md-right");
					var dateTime = post.comments[i].commentedAt;
					var time = dateTime.date.day + "." + dateTime.date.month + "." + dateTime.date.year + ". ";
					time += dateTime.time.hour + ":" + dateTime.time.minute + ":" + dateTime.time.second;
					commentedAtLabel.innerText = time;
					commentHeader.appendChild(commentedAtLabel);

					var comment = document.createElement("p");
					comment.classList.add("comment-text");
					comment.innerText = post.comments[i].text;

					commentDiv.appendChild(commentHeader);
					commentDiv.appendChild(comment);

					if (post.comments[i].imagePath != undefined) {
						var commentImg = document.createElement("img");
						commentImg.src = post.comments[i].imagePath;
						commentImg.classList.add("comment-img");
						commentDiv.append(commentImg);
					}
					var urlRegex = /(https?:\/\/[^\s]+)/g;
					comment.innerHTML = comment.innerText.replace(urlRegex, function(url) {
						return '<a href="' + url + '" target="_blank">' + url + '</a>';
					});
					commentDiv.appendChild(document.createElement("hr"));

					commentSectionDiv.appendChild(commentDiv);
				}
			}
		};
		xhttp.open("GET", "http://localhost:8080/user-posts?post_id=" + id, false);
		xhttp.send();

		var addCommentDiv = document.createElement("div");
		var form = document.createElement("form");
		form.enctype = "multipart/form-data";
		var img = document.createElement("img");
		img.src = document.getElementById("avatar").src;
		img.classList.add("add-comment-avatar");
		addCommentDiv.appendChild(img);

		var label = document.createElement("label");
		label.classList.add("add-comment-label");
		label.innerText = document.getElementById("user-name").innerText

		addCommentDiv.appendChild(label);
		var action = document.createElement("input");
		action.type = "hidden";
		action.name = "action";
		action.value = "add-comment";
		form.append(action);

		var postIdField = document.createElement("input");
		postIdField.type = "hidden";
		postIdField.name = "post_id";
		postIdField.value = id;
		form.append(postIdField);

		var commentTextField = document.createElement("input");
		commentTextField.classList.add("form-control");
		commentTextField.name = "text";
		commentTextField.type = "text";
		commentTextField.placeholder = "Add your comment here";

		addCommentDiv.appendChild(commentTextField);

		form.append(commentTextField);

		var chooseImageLabel = document.createElement("label");
		chooseImageLabel.htmlFor = "comment-image";
		chooseImageLabel.innerText = "Choose image\t";
		chooseImageLabel.htmlFor = "comment-image";
		form.appendChild(chooseImageLabel);

		var chooseImage = document.createElement("input");
		chooseImage.type = "file";
		chooseImage.name = "comment-image";
		chooseImage.id = "comment-image";
		chooseImage.accept = "image/*";

		form.appendChild(chooseImage);

		var addButton = document.createElement("a");
		addButton.innerText = "Add comment";
		addButton.classList.add("btn");
		addButton.classList.add("add-comment-button")
		addButton.classList.add("btn-info");
		addButton.classList.add("float-md-right");
		form.appendChild(addButton);
		addButton.onclick = () => {
			var formData = new FormData(form);
			xhttp = new XMLHttpRequest();
			xhttp.open("POST", "http://localhost:8080/add-comment", false);
			xhttp.send(formData);

			var btnComments = document.getElementById("add-comment-button-" + id);
			btnComments.click();
			showOrHideComments(id);
		}
		addCommentDiv.appendChild(form);
		commentTextField.onkeydown = (event) => {
			if (event.key === "Enter")
				addButton.click();
		}


		commentSectionDiv.appendChild(addCommentDiv);
		postDiv.appendChild(commentSectionDiv);
	}
}

getRSSPosts = () => {
	var postsHTML = "";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var postsDiv = document.getElementById("posts");
			Object.values(JSON.parse(this.responseText)).forEach(element => {

				var post = document.createElement("div");
				post.classList.add("post")
				var title = document.createElement("label");
				var pubDate = document.createElement("label");
				var link = document.createElement("a");
				link.href = element.link;
				link.innerText = "Link";
				link.classList.add("pub-date");
				pubDate.innerText = element.pubDate.split("+")[0];
				pubDate.classList.add("pub-date");
				title.classList.add("post-title");
				var content = document.createElement("p");
				content.classList.add("post-content")
				content.innerText = element.description
				title.innerText = element.title;
				var hr = document.createElement("hr");
				post.appendChild(title);
				post.appendChild(pubDate);
				post.appendChild(hr);
				post.appendChild(content);
				post.appendChild(link);
				postsHTML += post.outerHTML;
			});
			postsDiv.innerHTML += postsHTML;
		}
	};
	xhttp.open("GET", "http://localhost:8080/rss-posts", true);
	xhttp.send();
}

getNotifications = () => {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var notificationsDiv = document.getElementById("notifications");
			notificationsDiv.innerHTML = "";
			Object.values(JSON.parse(this.responseText)).forEach(element => {

				var notification = document.createElement("div");
				notification.classList.add("notification");
				notification.onclick = () => {
					var modal = document.getElementById("myModal");

					var span = document.getElementsByClassName("close")[0];

					modal.style.display = "block";

					span.onclick = function() {
						modal.style.display = "none";
					}

					window.onclick = function(event) {
						if (event.target == modal) {
							modal.style.display = "none";
						}
					}

					var description = document.getElementById("description-modal");
					description.innerText = element.description;

					var latitude = document.getElementById("latitude-modal");
					latitude.value = element.latitude;
					var longitude = document.getElementById("longitude-modal");
					longitude.value = element.longitude;

					var categories = document.getElementById("categories-modal");
					categories.innerHTML = "";
					for (i = 0; i < element.categories.length; ++i) {
						var li = document.createElement("li");
						li.innerText = element.categories[i].categoryName;
						li.classList.add("list-group-item");
						categories.appendChild(li);
					}
					var bl = { lat: element.latitude, lng: element.longitude };
					var map = new google.maps.Map(document.getElementById('map-modal'), { zoom: 16, center: bl });
					var marker = new google.maps.Marker({ position: bl });
					marker.setMap(map);

				}
				var text = document.createElement("p");
				text.classList.add("notification-text");
				text.innerText = element.description
				notification.appendChild(text);
				notificationsDiv.appendChild(notification);
			});
		}
	};
	xhttp.open("GET", "http://localhost:8080/warnings/urgent", true);
	xhttp.send();
}


textChange = () => {
	var textArea = document.getElementById("post-text");
	var result = YOUTUBE_URL_REGEX.exec(textArea.value);
	var attachmentsDiv = document.getElementById("attachments-div");
	if (attachmentsDiv.innerHTML.indexOf("img-attachment-div") != -1 || attachmentsDiv.innerHTML.indexOf("video") != -1)
		return;
	if (result != null) {
		var src = "https://www.youtube.com/embed/" + getYoutubeVideoID(result[0])
		
		if (attachmentsDiv.lastChild != null && attachmentsDiv.lastChild.nodeName == "IFRAME" && attachmentsDiv.lastChild.src == src)
			return;
		attachmentsDiv.innerHTML = "";
		var iframe = document.createElement("iframe");
		iframe.classList.add("new-post-yt");
		iframe.src = src;
		attachmentsDiv.appendChild(iframe);
		document.getElementById("post-type").value = "youtube_video";
	}
}

getYoutubeVideoID = (url) => {
	var youtu_be = YOUTU_BE_REGEX.exec(url);
	var video_id = "";
	if (youtu_be != null) {
		var index__of_slash = youtu_be[0].lastIndexOf("/");
		video_id = youtu_be[0].substring(index__of_slash + 1, youtu_be[0].length);
		var spacePosition = video_id.indexOf(" ");
		if (spacePosition != -1)
			video_id = video_id.substring(0, spacePosition);
	} else {
		video_id = url.split('v=')[1];
		var ampersandPosition = video_id.indexOf('&');
		var spacePosition = video_id.indexOf(" ");
		if (ampersandPosition != -1) {
			video_id = video_id.substring(0, ampersandPosition);
		}
		if (spacePosition != -1 && ampersandPosition > spacePosition)
			video_id = video_id.substring(0, spacePosition);
	}
	return video_id;
}

loadImages = (event) => {
	var attachmentsDiv = document.getElementById("attachments-div");
	attachmentsDiv.innerHTML = "";
	var div = document.createElement("div");
	div.classList.add("row");
	for (i = 0; i < event.target.files.length; ++i) {
		var img = document.createElement("img");
		img.classList.add("img-attachment");
		img.src = URL.createObjectURL(event.target.files[i]);
		img.onload = function() {
			URL.revokeObjectURL(img.src)
		}
		div.appendChild(img);
	}
	attachmentsDiv.appendChild(div);
	document.getElementById("post-type").value = "images";
	document.getElementById("video-chooser").value = null;
}

loadVideo = (event) => {
	var attachmentsDiv = document.getElementById("attachments-div");
	attachmentsDiv.innerHTML = "";
	var video = document.createElement("video");
	var source = document.createElement("source");
	video.classList.add("new-post-video");
	source.src = URL.createObjectURL(event.target.files[0]);
	source.onload = function() {
		URL.revokeObjectURL(source.src)
	}
	video.controls = true;
	video.autoplay = true;
	video.appendChild(source);
	attachmentsDiv.appendChild(video);
	video.load();
	document.getElementById("post-type").value = "video";
	document.getElementById("image-chooser").value = null;
}