<%@page import="danger.warning.system.beans.UserBean"%>
<%@page import="danger.warning.system.dto.User"%>
<%@page import="danger.warning.system.dto.Post"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" /> 
	<title>Danger Warning System</title>
	<script src="resources/js/post.js"></script>
	<link rel="stylesheet" href="resources/css/dashboard.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<%
	Post post = null;
	User user = null;
    if (request.getSession().getAttribute("user") == null){
    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
    		requestDispatcher.forward(request, response);
    	}
    else {
		post = (Post) request.getSession().getAttribute("post");
		request.getSession().removeAttribute("post");
		user = ((UserBean)request.getSession().getAttribute("user")).getUser();
    }
	%>
</head>
<body onload="showPost(<%= post.getId() %>, '<%= user.getName() + " " + user.getSurname() %>', '<%= user.getAvatar().substring(user.getAvatar().lastIndexOf('\\') + 1)%>')">	
	<div id="post-part">
	</div>
</body>
</html>