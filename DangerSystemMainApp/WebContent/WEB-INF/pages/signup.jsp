<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Danger Warning - Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="resources/css/signup.css" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <%
    if (request.getSession().getAttribute("user") != null) {
    	RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
    	requestDispatcher.forward(request, response);
    }
    %>
    <script>
    history.replaceState("", "", '/');
    </script>
</head>
<body>
	<div class="container-fluid">
 		<div class="form-row" style="margin-top:50px; margin-left:50px;">
			<img alt="Danger Logo" src="resources/images/danger.png" style="width:150px; height:150px;">
			<div class="form-group col-md-6">
				<h3>Danger Warning System</h3>
			</div>
		</div>
    	<form id="signup-form" action="auth" method="POST">
        	<div class="form-row">
            	<div class="form-group col-md-6">
                	<label for="name">Name</label>
                	<input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
            	</div>
            	<div class="form-group col-md-6">
                	<label for="surname">Surname</label>
                	<input type="text" class="form-control" name="surname" id="surname" placeholder="Surname" required>
            	</div>
        	</div>
        	<div class="form-group">
        		<label for="username">Username</label>
        		<input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
        	</div>
        	<div class="form-group">
        		<label for="email">Email</label>
        		<input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
        	</div>
        	<div class="form-row">
            	<div class="form-group col-md-6">
               		<label for="password">Password</label>
                	<input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
            	</div>
            	<div class="form-group col-md-6">
                	<label for="password2">Confirm Password</label>
                	<input type="password" class="form-control" name="password2" id="password2" placeholder="Confirm Password" required>
            	</div>
            	<div class="form-group">
            		<label style="color: red;">
            			<%= request.getSession().getAttribute("signup_msg") == null ? "" : request.getSession().getAttribute("signup_msg")%>
            		</label>
           		</div>
        	</div>
        	<div class="text-right">
        		<a style="width: 150px" class="btn btn-secondary" href="auth" role="button">Cancel</a>
        		<button style="width: 150px" type="submit" class="btn btn-success">Sign Up</button>
        	</div>
        	<input type="hidden" name="action" value="signup">
    	</form>
    </div>
</body>
</html>