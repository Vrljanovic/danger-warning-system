<%@page import="danger.warning.system.dto.UserStatus"%>
<%@page import="danger.warning.system.beans.UserBean"%>
<%@page import="danger.warning.system.dto.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Account Settings</title>
	<link rel="stylesheet" href="resources/css/account-settings.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <script src="resources/js/account-settings.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script>
    history.replaceState("", "", '/');
    </script>
    <%
    User user = null;
    if (request.getSession().getAttribute("user") == null){
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
		requestDispatcher.forward(request, response);
	}else {
    	UserBean userBean = (UserBean)request.getSession().getAttribute("user"); 
    	user = userBean.getUser();
    	if (user.getStatus().equals(UserStatus.ACTIVE)) {
    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
    		requestDispatcher.forward(request, response);
    	}
	}
    %>
</head>
<body>
	<div class="container-fluid">
		<form action="auth" method="POST" enctype="multipart/form-data">
			<input type="hidden" id="action" name="action" value="change_info">
        	<input type="hidden" id=avatar-val name="avatar-val" value="images/avatar.svg">
			<div class="form-row">
				<div id="personal-settings" class="form-group col-md-6 col-sm-12">
						<div class="form-row">
            				<div class="form-group col-md-6">
                				<label for="name">Name</label>
                				<input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<%= user != null ? user.getName() : ""%>"disabled>
            				</div>
            				<div class="form-group col-md-6">
                				<label for="surname">Surname</label>
                				<input type="text" class="form-control" name="surname" id="surname" placeholder="Surname" value="<%= user != null ? user.getSurname() : ""%>" disabled>
            				</div>
           				</div>
            			<div class="form-group">
        					<label for="username">Username</label>
        					<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<%= user != null ? user.getUsername() : ""%>" disabled>
        				</div>
        				<div class="form-group">
        					<label for="email">Email</label>
        					<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<%= user != null ? user.getEmail() : ""%>" disabled>
        				</div>
        				<div class="form-group">
        					<label for="country">Country</label>
        					<select class="form-control" id="country" name="country" required="required" onchange="addRegions()" ></select>
        				</div>
        				<div class="form-group">
        					<label for="country">Region</label>
        					<select class="form-control" id="region" name="region" required="required" onchange="addCities()"></select>
        				</div>
        				<div class="form-group">
        					<label for="country">City</label>
        					<select class="form-control" id="city" name="city" required="required" ></select>
        				</div>
				</div>
				<div id="other-info" class="form-group col-md-6 col-sm-12">
						<div class="form-group" style="width: 200px; height:200px">
        					<img style="margin-top: 10px;"class="img-thumbnail" alt="Avatar" id="avatar" name="avatar" src="resources/images/avatar.svg">
        				</div>
        				<div class="form-group">
        					<input type="file" id="file-input" name="file-input" class="form-control-file" placeholder="Choose Avatar" title="Choose Avatar" accept=".jpg, .png, .svg, .jpeg" onchange="loadFile(event)">
        				</div>
        				<div class="form-group">
        					<div class="custom-control custom-checkbox">
  								<input type="checkbox" class="custom-control-input" id="email-notifications" name="email-notifications">
  								<label class="custom-control-label" for="email-notifications">Send Notifications via Email</label>
							</div>
        				</div>
        				<div class="form-group">
        					<div class="custom-control custom-checkbox">
  								<input type="checkbox" class="custom-control-input" id="app-notifications" name='app-notifications'>
  								<label class="custom-control-label" for="app-notifications">Send Notifications in Application</label>
							</div>
        				</div>
        				<div class="form-group">
        					<button type="submit" id="submit-button" class="btn btn-success btn-large btn-block">Confirm</button>
        				</div>
					</div>
			</div>
		</form>
	 </div>
</body>
</html>