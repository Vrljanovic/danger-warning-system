<%@page import="danger.warning.system.dao.WarningCategoryDAO"%>
<%@page import="danger.warning.system.dto.WarningCategory"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" /> 
	<title>Add New Warning</title>
	<link rel="stylesheet" href="resources/css/new-warning.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <script src="resources/js/new-warning.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <%
    if (request.getSession().getAttribute("user") == null){
    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
    		requestDispatcher.forward(request, response);
    	}
    %>
    
</head>
<body>
	<div class="content">
		<h2>Add New Warning</h2><br>
		<form action="auth" method="POST">
			<input type="hidden" id="action" name="action" value="add-warning">
			<input type="hidden" id="longitude" name="longitude" value="225883">
			<input type="hidden" id="latitude" name="latitude" value="225883">
			<label for="description">Description</label><br>
			<textarea id="description" name="description" required></textarea><br>
			<div class="custom-control custom-checkbox">
  				<input type="checkbox" class="custom-control-input" id="is-urgent" name="is-urgent">
  				<label class="custom-control-label" for="is-urgent" id="is-urgent-label">Is Urgent</label>
			</div><br>
			<label for="category">Category (Choose one or more)</label><br>
			<select id="category" name="category" class="custom-select" multiple required>
				<% 
					List<WarningCategory> categories = new WarningCategoryDAO().getAllCategories();
					for (WarningCategory category: categories) {
				%>
					<option value="<%= category.getId() %>"><%= category.getCategoryName() %></option>
				<%
					}
				%>
			</select><br>
			<label for="map">Choose Location (optional)</label>
			<div id="map"></div>
			<input id="submit-button" type="submit" class="btn btn-primary"></input>
		</form>
	</div>
	<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxPAGB54pfbFTpveFSiQ18HuHtr_ssVnw&callback=initMap"></script>
</body>
</html>