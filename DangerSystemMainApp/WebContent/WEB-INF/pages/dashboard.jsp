<%@page import="danger.warning.system.dto.UserStatus"%>
<%@page import="danger.warning.system.beans.UserBean"%>
<%@page import="danger.warning.system.dto.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" /> 
	<title>Danger Warning System - Dashboard</title>
	<link rel="stylesheet" href="resources/css/dashboard.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <script src="resources/js/dashboard.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <% 
    User user = null;
    String avatar = null;
    if (request.getSession().getAttribute("user") == null){
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
		requestDispatcher.forward(request, response);
		
	}else {
    	UserBean userBean = (UserBean)request.getSession().getAttribute("user"); 
    	user = userBean.getUser();
    	if (!user.getStatus().equals(UserStatus.ACTIVE)){
    		request.getSession().invalidate();
    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
    		requestDispatcher.forward(request, response);
    	}
    	avatar = user.getAvatar();
	}
    %>
</head>
<body onload="init('<%= user.getCountry()%>', '<%= user.getRegion()%>', '<%= user.getCity()%>')">
	<div class="topnav">
		<a href="auth" style="margin-left:2vw; ">Dashboard</a>
		<a href="auth?action=new-warning" style="margin-left:1vw; ">Add New Warning</a>
		<div class="topnav-right">
			<a href="auth?action=logout" style="margin-right:2vw; ">Logout (<%= user.getUsername() %>)</a>
		</div>
	</div>
	<div id="main-page" >
		<div id="left">
			<div id="image-div">
				<img id="avatar" src="<%= avatar %>"/>
			</div>
			<label id="user-name"> <%= user.getName() + " " + user.getSurname() %> </label><br>
			<label id="login-count"> Logins: <%= user.getLoginCount() %> </label>
			<%
				if (user.isAppNotifications()) {
			%>
			<label id="notification-label"	>Notifications:</label>
			<div id="notifications">
				
			</div>
			<% } %>
		</div>
		<div id="middle">
			<div id="new-post">
				<form action="auth" method="POST" enctype="multipart/form-data">
					<div id="textarea-div">
						<textarea id="post-text" name="post-text" placeholder="Insert text, link or youtube video link" oninput="textChange()"></textarea>
					</div>
					<div id="attachments-div"></div>
					<div id="chooser-div">
						<label for="image-chooser">Choose images:</label>
						<input type="file" multiple="multiple" id="image-chooser" name="image-chooser" accept="image/*" onchange="loadImages(event)"/>
						<label for="video-chooser">or choose video:	</label>
						<input type="file" id="video-chooser" name="video-chooser" accept="video/*" onchange="loadVideo(event)"/>
					</div>
					<input id="add-post-button" type="submit" class="btn btn-success" />
					<input type="hidden" name="action" id="action" value="new-post" />
					<input type="hidden" name="post-type" id="post-type" value="text"/>
				</form>
			</div>
			<div id="posts">
			</div>
		</div>
		<div id="right">
		</div>
	</div>
	<%
		if (user.isAppNotifications()) {
	%>
	<div id="myModal" class="modal">
		<div class="modal-content">
    		<span class="close">&times;</span>
    		<h5 style="float:center;align-self: center; margin-top: 2vh;">Urgent Warning Details</h5><hr/>
    		<label for="description-modal">Description:</label><br>
			<textarea id="description-modal" name="description-modal" readonly="readonly"></textarea>
			<label for="latitude-modal">Latitude:</label>
			<input type="text" id="latitude-modal" name="latitude-modal">
			<label for="longitude-modal">Longitude:</label>
			<input type="text" id="longitude-modal" name="longitude-modal">
			<label for="categories-modal">Categories:</label>
			<ul id="categories-modal" class="list-group"></ul>
			<label for="map-modal">Map:</label>
			<div id="map-modal"></div>
  		</div>
	</div>
	<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxPAGB54pfbFTpveFSiQ18HuHtr_ssVnw"></script>
	<%
		}
	%>
</body>
</html>