<%@page import="danger.warning.system.dto.UserStatus"%>
<%@page import="danger.warning.system.dto.User"%>
<%@page import="danger.warning.system.beans.UserBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <title>Danger Warning - Login</title>
    <link rel="stylesheet" href="resources/css/login.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <%
    	UserBean userBean = (UserBean)request.getSession().getAttribute("user");
    	User user = userBean == null? null: userBean.getUser();
    	if ( user != null && user.getStatus().equals(UserStatus.ACTIVE)) {
    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
    		requestDispatcher.forward(request, response);
    	}
    %>
    <script>
    history.replaceState("", "", '/');
    </script>
</head>
<body>
	<div class="login">
    	<h2>Danger Warning System</h2>
        <form method="POST" action="auth">
        	<input type="text" name="username" id="username" placeholder="Username" required="required" />
            <input type="password" name="password" id="password" placeholder="Password" required="required" />
            <label id="msg" style="color: red;"><%= request.getSession().getAttribute("login_msg") == null ? "" : request.getSession().getAttribute("login_msg") %></label>   
            <button class="btn btn-primary btn-block btn-large">Login</button>
            <input type="hidden" id="action" name="action" value="login"/>
            <br>
            <br>
        </form>
        <form method="POST" action="auth">
        	<label>First time here?</label><br>
            <button class="btn btn-success btn-block btn-large">Sign Up</button>
            <input type="hidden" name="action" value="show_signup_form"/>
        </form>
     </div>
</body>
</html>