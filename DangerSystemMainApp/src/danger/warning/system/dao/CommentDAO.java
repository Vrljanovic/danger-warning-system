package danger.warning.system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.Comment;
import danger.warning.system.dto.Post;
import danger.warning.system.dto.User;

public class CommentDAO {

	public void addComment(Post post, User user, Comment comment) {
		String sql = "insert into comment values (?, ?, ?, ?, ?)";
		
		Connection connection = null;
		
		try {
			connection =  ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement  =  connection.prepareStatement(sql);
			statement.setInt(1, user.getId());
			statement.setInt(2, post.getId());
			statement.setString(3, comment.getCommentedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			statement.setString(4, comment.getText());
			statement.setString(5, comment.getImagePath());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
	}
	
	public List<Comment> getCommentsForPostId(int id) {
		List<Comment> comments = new LinkedList<Comment>();
		
		String sql = "select* from comment inner join post on post_id = id where id = ? order by commented_at asc";
		
		Connection connection = null;
		try {
			connection =  ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement  =  connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			UserDAO userDAO = new UserDAO();
			while(resultSet.next()) {
				String text = resultSet.getString("text");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime commentedAt = LocalDateTime.parse(resultSet.getString("commented_at"), formatter);
				int userId = resultSet.getInt("user_id");
				String imagePath = resultSet.getString("image_path");
				comments.add(new Comment(userDAO.getUserByUniqueField("id", userId + ""), commentedAt, text, imagePath));
			}
			resultSet.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return comments;
	}
}
