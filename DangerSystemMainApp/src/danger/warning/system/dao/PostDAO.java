package danger.warning.system.dao;

import java.util.LinkedList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.Post;
import danger.warning.system.dto.PostType;

public class PostDAO {

	public void addPost(Post post) {
		String sql = "insert into post values (null, ?, ?, ?, ?)";

		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, post.getUser().getId());
			statement.setString(2, post.getPostedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			statement.setString(3, post.getPostType().getPostType());
			statement.setString(4, post.getText());

			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next())
				post.setId(resultSet.getInt(1));
			resultSet.close();
			PostAttachmentDAO postAttachmentDAO = new PostAttachmentDAO();
			post.getAttachments().forEach(attachment -> postAttachmentDAO.addAttachment(post, attachment));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
	}

	public List<Post> getAllPosts() {
		List<Post> posts = new LinkedList<Post>();
		
		String sql = "select * from post inner join user on user.id = user_id where status = 'ACTIVE' order by posted_at desc";
		
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement =  connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			
			PostAttachmentDAO postAttachmentDAO = new PostAttachmentDAO();
			CommentDAO commentDAO = new CommentDAO();
			UserDAO userDAO = new UserDAO();
			while(resultSet.next()) {
				int id = resultSet.getInt("id");
				int userId = resultSet.getInt("user_id");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime postedAt = LocalDateTime.parse(resultSet.getString("posted_at"), formatter);
				PostType postType = PostType.valueOf(resultSet.getString("type"));
				String text = resultSet.getString("text");
				posts.add(new Post(id, userDAO.getUserByUniqueField("id", userId + ""), postedAt, postType, text, 
						postAttachmentDAO.getAttachmentsForPostId(id), commentDAO.getCommentsForPostId(id)));
			}
			
			resultSet.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return posts;
	}
	
	public Post getPostById(int id) {
		Post post = null;
		
		String sql = "select * from post inner join user on user_id = user.id where post.id = ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement  = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			PostAttachmentDAO postAttachmentDAO = new PostAttachmentDAO();
			CommentDAO commentDAO = new CommentDAO();
			UserDAO userDAO = new UserDAO();
			
			if (resultSet.next()) {
				int userId = resultSet.getInt("user_id");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime postedAt = LocalDateTime.parse(resultSet.getString("posted_at"), formatter);
				PostType postType = PostType.valueOf(resultSet.getString("type"));
				String text = resultSet.getString("text");
				post = new Post(id, userDAO.getUserByUniqueField("id", userId + ""), postedAt, postType, text, 
						postAttachmentDAO.getAttachmentsForPostId(id), commentDAO.getCommentsForPostId(id));
			}
			
			resultSet.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		
		return post;
	}
}
