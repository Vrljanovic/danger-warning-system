package danger.warning.system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.Warning;
import danger.warning.system.dto.WarningCategory;

public class WarningDAO {
	
	//String sql = "select warning_id, description, is_urgent, longitude, latitude, category_id, category from warning inner join" +
	//	"warning_category on warning.id = warning_id inner join category on category.id = category_id;";
	
	public boolean addWarning(Warning warning) {
		boolean success = true;
		
		String sql = "insert into warning values (null, ?, ?, ?, ?)";
		
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, warning.getDescription());
			statement.setBoolean(2, warning.isUrgent());
			if(warning.getLongitude() == null)
				statement.setNull(3,  Types.DOUBLE);
			else
				statement.setDouble(3, warning.getLongitude());
			if(warning.getLatitude() == null)
				statement.setNull(4,  Types.DOUBLE);
			else
				statement.setDouble(4, warning.getLatitude());;
			
			statement.executeUpdate();
			
			ResultSet resultSet = statement.getGeneratedKeys();
			if(resultSet.next())
				warning.setId(resultSet.getInt(1));
			resultSet.close();
			WarningCategoryDAO warningCategoryDAO = new WarningCategoryDAO();
			for(WarningCategory category : warning.getCategories())
				warningCategoryDAO.addCategory(warning, category);
			
		} catch (SQLException e) {
			e.printStackTrace();
			success = false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return success;
	}
	

	public List<Warning> getUrgentWarnings() {
		
		List<Warning> warnings = new LinkedList<Warning>();
		
		String sql = "select * from warning where is_urgent = 1 order by id desc";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			WarningCategoryDAO warningCategoryDAO = new WarningCategoryDAO();
			while (resultSet.next() ) {
				Integer id = resultSet.getInt("id");
				String description = resultSet.getString("description");
				boolean isUrgent = resultSet.getBoolean("is_urgent");
				Double longitude = resultSet.getDouble("longitude");
				Double latitude = resultSet.getDouble("latitude");
				warnings.add(new Warning(id, description, longitude, latitude, isUrgent, warningCategoryDAO.getCategoriesByWarningId(id)));
			}
			resultSet.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}		
		
		return warnings;
	}
	
}
