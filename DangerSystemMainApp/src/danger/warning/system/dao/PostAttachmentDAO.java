package danger.warning.system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.Post;
import danger.warning.system.dto.PostAttachment;

public class PostAttachmentDAO {
	
	public void addAttachment(Post post, PostAttachment attachment) {
		
		String sql = "insert into post_attachment values(?, ?, ?)";
		
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, post.getId());
			statement.setString(2, attachment.getPath());
			statement.setString(3, attachment.getType());
			
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
	}

	public List<PostAttachment> getAttachmentsForPostId(int id) {
		List<PostAttachment> attachments = new LinkedList<PostAttachment>();
		
		String sql = "select path, type from post_attachment where post_id = ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) 
				attachments.add(new PostAttachment(resultSet.getString("path"), resultSet.getString("type")));
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return attachments;
	}
	
}
