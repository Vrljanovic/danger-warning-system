package danger.warning.system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.Warning;
import danger.warning.system.dto.WarningCategory;

public class WarningCategoryDAO {

	public void addCategory(Warning warning, WarningCategory category) {
		String sql = "insert into warning_category values (?, ?)";
		
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, warning.getId());
			statement.setInt(2, category.getId());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
	}
	
	public List<WarningCategory> getAllCategories() {
		List<WarningCategory> categories = new LinkedList<WarningCategory>();
		
		String sql = "select * from category";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				int id = resultSet.getInt("id");
				String categoryName = resultSet.getString("category");
				categories.add(new WarningCategory(id, categoryName));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return categories;
	}
	
	public WarningCategory getCategoryById(int id) {
		WarningCategory category = null;
		
		String sql = "select * from category where id = ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
				category = new WarningCategory(id, resultSet.getString("category"));
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return category;
	}
	
	public List<WarningCategory> getCategoriesByWarningId(int id) {
		List<WarningCategory> categories = new LinkedList<WarningCategory>();
		
		String sql = "select id, category from warning_category inner join category on id = category_id where warning_id = ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) 
				categories.add(new WarningCategory(resultSet.getInt("id"), resultSet.getString("category")));
			resultSet.close();
			
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return categories;
	}
}
