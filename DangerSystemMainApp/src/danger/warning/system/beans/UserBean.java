package danger.warning.system.beans;

import java.io.Serializable;

import danger.warning.system.dao.UserDAO;
import danger.warning.system.dto.User;

public class UserBean implements Serializable {
	
	private static final long serialVersionUID = -7504677322194847448L;
	
	private User user = new User();
	private boolean isLoggedIn = false;
	
	public String login(String username, String password) {
		User user = null;
		UserDAO userDAO = new UserDAO();
		if((user = userDAO.checkCredentials(username, password)) != null) {
			if("PENDING_APPROVAL".equals(user.getStatus().toString()))
					return "Your account is still not activated.";
			if("BLOCKED".equals(user.getStatus().toString()))
				return "Contact administrator.";
			this.user = user;
			this.isLoggedIn = true;
			userDAO.addLoginInfo(user);
			userDAO.incrementLoginCount(user);
			return "OK";
		}
		return "Wrong username and password combination";
	}
	
	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public void logout() {
		new UserDAO().setUserInactive(this.user);
		this.user = new User();
		isLoggedIn = false;
	}

	public User getUser() {
		return user;
	}

	public boolean isUserNameAllowed(String username) {
		return new UserDAO().isUsernameUsed(username);
	}
	
	public String add(User user) {
		this.user = user;
		UserDAO userDAO = new UserDAO();
		if (userDAO.isUsernameUsed(user.getUsername()))
			return "User with same username already exists";
		if (userDAO.isEmailUsed(user.getEmail()))
			return "User with same email already exists";
		if (userDAO.addUser(user))
			return "OK";
		return "Database Error. Try again later";
	}
	
	public boolean update() {
		return new UserDAO().updateUserData(this.user);
	}

}
