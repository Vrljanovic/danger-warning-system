package danger.warning.system.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import danger.warning.system.beans.UserBean;
import danger.warning.system.dao.PostDAO;
import danger.warning.system.dto.Post;

@WebServlet("/post")
public class PostController extends HttpServlet {

	private static final long serialVersionUID = -8778559427032668839L;
	private static final String DASHBOARD = "WEB-INF/pages/dashboard.jsp";
	private static final String POST_PAGE = "WEB-INF/pages/post.jsp";
	private static final String LOGIN = "WEB-INF/pages/login.jsp";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String post_id = req.getParameter("post_id");
		String address = POST_PAGE;
		UserBean userBean = (UserBean) req.getSession().getAttribute("user");
		if (userBean == null)
			address = LOGIN;
		else {
			if (post_id == null)
				address = DASHBOARD;
			else {
				try {
					int id = Integer.parseInt(post_id);
					Post post = new PostDAO().getPostById(id);
					if (post == null)
						address = DASHBOARD;
					else
						req.getSession().setAttribute("post", post);
				} catch (NumberFormatException e) {
					address = DASHBOARD;
				}
			}
		}
		RequestDispatcher requestDispatcher = req.getRequestDispatcher(address);
		requestDispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
