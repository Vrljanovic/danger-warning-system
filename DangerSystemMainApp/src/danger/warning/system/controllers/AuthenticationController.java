package danger.warning.system.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import danger.warning.system.beans.UserBean;
import danger.warning.system.dao.PostDAO;
import danger.warning.system.dao.UserDAO;
import danger.warning.system.dao.WarningCategoryDAO;
import danger.warning.system.dao.WarningDAO;
import danger.warning.system.dto.Post;
import danger.warning.system.dto.PostAttachment;
import danger.warning.system.dto.PostType;
import danger.warning.system.dto.User;
import danger.warning.system.dto.UserRole;
import danger.warning.system.dto.UserStatus;
import danger.warning.system.dto.Warning;
import danger.warning.system.dto.WarningCategory;
import danger.warning.system.mail.MailSender;

@WebServlet("/auth")
@MultipartConfig
public class AuthenticationController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Random random = new Random();

	public AuthenticationController() {
		super();
	}

	private static final String LOGIN = "WEB-INF/pages/login.jsp";
	private static final String SIGN_UP = "WEB-INF/pages/signup.jsp";
	private static final String ACCOUNT_SETTINGS = "WEB-INF/pages/account-settings.jsp";
	private static final String DASHBOARD = "WEB-INF/pages/dashboard.jsp";
	private static final String NEW_WARNING = "WEB-INF/pages/new-warning.jsp";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String action = request.getParameter("action");
		action = action == null ? "" : action;
		String address = LOGIN;
		HttpSession session = request.getSession();
		session.setAttribute("login_msg", "");
		session.setAttribute("signup_msg", "");
		if ("".equals(action)) {
			address = LOGIN;
			UserBean userBean = (UserBean) request.getSession().getAttribute("user");
			if (userBean != null && userBean.isLoggedIn())
				address = DASHBOARD;

		} else if ("show_signup_form".equals(action))
			address = SIGN_UP;
		else if ("change_info".equals(action)) {
			UserBean userBean = (UserBean) session.getAttribute("user");
			address = ACCOUNT_SETTINGS;

			String country = request.getParameter("country");
			String region = request.getParameter("region");
			String city = request.getParameter("city");
			boolean emailNotification = request.getParameter("email-notifications") != null;
			boolean appNotification = request.getParameter("app-notifications") != null;
			String avatar = request.getParameter("avatar-val");
			String avatarPath = avatar;
			if ("".equals(avatar)) {
				Part part = request.getPart("file-input");
				String fileName = userBean.getUser().getUsername() + "_" + part.getSubmittedFileName();
				byte[] avatarBytes = part.getInputStream().readAllBytes();
				avatarPath = getServletContext().getRealPath("user_avatars") + File.separator + fileName;
				Path path = Path.of(avatarPath);
				avatarPath = "user_avatars" + File.separator + fileName;
				Files.write(path, avatarBytes, StandardOpenOption.CREATE);
			}
			userBean.getUser().setCountry(country);
			userBean.getUser().setRegion(region);
			userBean.getUser().setCity(city);
			userBean.getUser().setAvatar(avatarPath);
			userBean.getUser().setEmailNotifications(emailNotification);
			userBean.getUser().setAppNotifications(appNotification);
			if (userBean.update()) {
				request.getSession().invalidate();
				userBean.logout();
				address = LOGIN;

			}
		} else if ("login".equals(action)) {
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			UserBean userBean = new UserBean();
			String msg = userBean.login(username, password);
			if (!"OK".equals(msg))
				session.setAttribute("login_msg", msg);
			if (userBean.isLoggedIn()) {
				address = DASHBOARD;
				session.setAttribute("user", userBean);
			}

		} else if ("signup".equals(action)) {

			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String username = request.getParameter("username");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");

			boolean canSignUp = true;
			address = SIGN_UP;
			if (password.length() < 6) {
				request.getSession().setAttribute("signup_msg", "Password must have minimum 6 characters");
				canSignUp = false;
			}

			if (!password.equals(password2)) {
				request.getSession().setAttribute("signup_msg", "Passwords do not match");
				canSignUp = false;
			}
			if (canSignUp) {
				String passwordHash = UserDAO.getSHA256Hash(password);
				User user = new User(null, name, surname, username, email, passwordHash, "", "", "", "", false, false,
						0, UserRole.CLASSIC_USER, UserStatus.PENDING_APPROVAL);
				UserBean userBean = new UserBean();
				String result = userBean.add(user);
				if ("OK".equals(result)) {
					session.setAttribute("user", userBean);
					address = ACCOUNT_SETTINGS;
				} else
					session.setAttribute("signup_msg", result);
			}
		} else if ("logout".equals(action)) {
			UserBean userBean = (UserBean) request.getSession().getAttribute("user");
			if (userBean != null)
				userBean.logout();
			request.getSession().invalidate();
			address = LOGIN;
		} else if ("new-warning".equals(action)) {
			address = NEW_WARNING;
		} else if ("add-warning".equals(action)) {

			String description = request.getParameter("description");
			boolean isUrgent = request.getParameter("is-urgent") != null;
			Double longitude = Double.parseDouble(request.getParameter("longitude"));
			Double latitude = Double.parseDouble(request.getParameter("latitude"));

			longitude = longitude == 225883 ? null : longitude;
			latitude = latitude == 225883 ? null : latitude;

			String[] categoryIds = request.getParameterValues("category");
			List<WarningCategory> categories = new LinkedList<>();
			WarningCategoryDAO warningCategoryDAO = new WarningCategoryDAO();
			for (String category : categoryIds)
				categories.add(warningCategoryDAO.getCategoryById(Integer.parseInt(category)));

			Warning warning = new Warning(null, description, longitude, latitude, isUrgent, categories);
			new WarningDAO().addWarning(warning);

			UserBean userBean = (UserBean) request.getSession().getAttribute("user");
			if (isUrgent && userBean.getUser().isEmailNotifications())
				MailSender.send(userBean.getUser(), warning);
			address = DASHBOARD;
		} else if ("new-post".equals(action)) {
			String postText = request.getParameter("post-text");
			String postType = request.getParameter("post-type");
			UserBean userBean = (UserBean) request.getSession().getAttribute("user");
			Post post = new Post(null, userBean.getUser(), LocalDateTime.now(), PostType.TEXT_OR_LINK, postText, null,
					null);
			boolean canPost = true;

			if ("images".equals(postType)) {
				List<PostAttachment> attachments = new LinkedList<PostAttachment>();
				for (Part part : request.getParts()) {
					if (part.getName().equals("image-chooser")) {
						if ("".equals(part.getSubmittedFileName()))
							canPost = false;
						attachments.add(createAttachment(part.getInputStream().readAllBytes(), "IMAGE",
								part.getSubmittedFileName()));
					}
				}
				post.setAttachments(attachments);
				post.setPostType(PostType.IMAGE);
			} else if ("video".equals(postType)) {
				Part videoChooser = request.getPart("video-chooser");
				if ("".equals(videoChooser.getSubmittedFileName()))
					canPost = false;
				else {
					List<PostAttachment> attachments = new LinkedList<PostAttachment>();

					attachments.add(createAttachment(videoChooser.getInputStream().readAllBytes(), "VIDEO",
							videoChooser.getSubmittedFileName()));
					post.setAttachments(attachments);
					post.setPostType(PostType.VIDEO);
				}
			} else if ("youtube_video".equals(postType))
				if (postText.isEmpty())
					canPost = false;
				else
					post.setPostType(PostType.YOUTUBE_VIDEO);
			else {
				if (postText.isEmpty())
					canPost = false;
			}
			if (canPost)
				new PostDAO().addPost(post);
			address = DASHBOARD;
		} 
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(address);
		requestDispatcher.forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	private PostAttachment createAttachment(byte[] bytes, String type, String submittedFileName) throws IOException {
		String extension = submittedFileName.substring(submittedFileName.lastIndexOf('.') + 1);
		String fileName = System.currentTimeMillis() + "_" + random.nextInt(Integer.MAX_VALUE) + "." + extension;
		String folder = "IMAGE".equals(type) ? "posted_images" : "posted_videos";
		String filePath = getServletContext().getRealPath(folder) + File.separator + fileName;
		Path path = Path.of(filePath);
		filePath = folder + File.separator + fileName;
		Files.write(path, bytes, StandardOpenOption.CREATE);
		return new PostAttachment(filePath, type);

	}
}
