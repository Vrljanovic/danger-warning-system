package danger.warning.system.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import danger.warning.system.dao.PostDAO;
import danger.warning.system.dto.Post;

@WebServlet("/user-posts")
public class UserPostController extends HttpServlet {

	private static final long serialVersionUID = -175875659586457063L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8");

		String post_id = req.getParameter("post_id");
		if (post_id != null) {
			try {
				int id = Integer.parseInt(post_id);
				Post post = new PostDAO().getPostById(id);
				
				if (post == null)
					resp.getWriter().println();
				else
					resp.getWriter().println(new Gson().toJson(post));
			} catch (NumberFormatException e) {
				resp.getWriter().println();
			}
		}
		else
			resp.getWriter().println(new Gson().toJson(new PostDAO().getAllPosts()));
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
