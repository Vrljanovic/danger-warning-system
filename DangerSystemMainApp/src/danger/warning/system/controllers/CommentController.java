package danger.warning.system.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import danger.warning.system.beans.UserBean;
import danger.warning.system.dao.CommentDAO;
import danger.warning.system.dao.PostDAO;
import danger.warning.system.dto.Comment;
import danger.warning.system.dto.User;

@MultipartConfig
@WebServlet("/add-comment")
public class CommentController extends HttpServlet {

	private static final long serialVersionUID = 2175782425902048965L;
	private static Random random = new Random();

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String text = request.getParameter("text");
		String post_id = request.getParameter("post_id");
		int postId = Integer.parseInt(post_id);
		User user = ((UserBean) request.getSession().getAttribute("user")).getUser();
		Part part = request.getPart("comment-image");
		String filename = part.getSubmittedFileName();
		String imgPath = null;
		if (!"".equals(filename)) {
			byte[] bytes = part.getInputStream().readAllBytes();
			filename = System.currentTimeMillis() + "_" + random.nextInt(Integer.MAX_VALUE) + "."
					+ filename.substring(filename.lastIndexOf('.') + 1);
			String path = getServletContext().getRealPath("posted_images") + File.separator + filename;
			imgPath = "posted_images\\" + filename;
			Files.write(Path.of(path), bytes, StandardOpenOption.CREATE);
		}
		if (!"".equals(part.getSubmittedFileName()) || !"".equals(text)) {
		Comment comment = new Comment(user, LocalDateTime.now(), text, imgPath);
		new CommentDAO().addComment(new PostDAO().getPostById(postId), user, comment);
		}
	}

}
