package danger.warning.system.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import danger.warning.system.rss.Feed;
import danger.warning.system.rss.RSSFeedParser;

@WebServlet("/rss-posts")
public class RSSPostController extends HttpServlet {

	private static final long serialVersionUID = -614036054640275854L;
	
	private static final String URI = "https://europa.eu/newsroom/calendar.xml_en?field_nr_events_by_topic_tid=151";

	public RSSPostController() {
		super();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8");
		RSSFeedParser parser = new RSSFeedParser(URI);
		Feed feed = parser.readFeed();
		Gson gson = new Gson();
		resp.getWriter().println(gson.toJson(feed.getMessages()));
	}
}
