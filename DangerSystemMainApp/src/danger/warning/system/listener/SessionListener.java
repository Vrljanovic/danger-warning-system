package danger.warning.system.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import danger.warning.system.beans.UserBean;

@WebListener
public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		
		HttpSession session =  se.getSession();
		UserBean userBean = (UserBean)session.getAttribute("user");
		if (userBean != null)
			userBean.logout();
	}
}
