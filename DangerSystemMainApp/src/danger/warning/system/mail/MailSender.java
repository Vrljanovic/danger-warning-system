package danger.warning.system.mail;

import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import danger.warning.system.dto.User;
import danger.warning.system.dto.Warning;

public class MailSender {
	private static String mailFrom;
	private static String username;
	private static String password;
	private static Properties properties;
	static {
		properties = new Properties();
		ResourceBundle bundle = PropertyResourceBundle.getBundle("danger.warning.system.mail.mail");
		properties.put("mail.smtp.auth", bundle.getString("mail.smtp.auth"));
		properties.put("mail.smtp.ssl.enable", bundle.getString("mail.smtp.ssl.enable"));
		properties.put("mail.smtp.host", bundle.getString("mail.smtp.host"));
		properties.put("mail.smtp.port", bundle.getString("mail.smtp.port"));
		mailFrom = bundle.getString("mail_from");
		username = bundle.getString("username");
		password = bundle.getString("password");
	}

	static public void send(User userTo, Warning warning) {
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailFrom));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(userTo.getEmail()));
			message.setSubject("New Urgent Warning");

			String text = "Hello " + userTo.getName() + " " + userTo.getSurname() + "\n"
					+ "We need to warn you about potential danger.\n" + "Description:\n" + warning.getDescription() + "\n";
			text += warning.getLatitude() == null || warning.getLongitude() == null ? "" : "Location: " + warning.getLatitude() + ", " + warning.getLongitude() + "\n";
			text += "Categories: " + warning.getCategories();

			message.setText(text);
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
