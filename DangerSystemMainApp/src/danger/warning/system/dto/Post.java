package danger.warning.system.dto;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class Post {

	private Integer id;
	private User user;
	private LocalDateTime postedAt;
	private PostType postType;
	private String text;
	List<PostAttachment> attachments;
	List<Comment> comments;

	public Post() {
		this.id = null;
		this.user = null;
		this.postedAt = LocalDateTime.now();
		this.postType = PostType.TEXT_OR_LINK;
		this.text = "";
		attachments = new LinkedList<PostAttachment>();
		comments = new LinkedList<Comment>();
	}

	public Post(Integer id, User user, LocalDateTime postedAt, PostType postType, String text, List<PostAttachment> attachments, List<Comment> comments) {
		this.id = id;
		this.user = user;
		this.postedAt = postedAt;
		this.postType = postType;
		this.text = text;
		this.attachments = new LinkedList<PostAttachment>();
		this.comments = new LinkedList<Comment>();
		if (attachments != null)
			this.attachments.addAll(attachments);
		if(comments != null)
			this.comments.addAll(comments);
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDateTime getPostedAt() {
		return postedAt;
	}

	public void setPostedAt(LocalDateTime postedAt) {
		this.postedAt = postedAt;
	}

	public PostType getPostType() {
		return postType;
	}

	public void setPostType(PostType postType) {
		this.postType = postType;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public List<PostAttachment> getAttachments() {
		return attachments;
	}
	
	public void setAttachments(List<PostAttachment> attachments) {
		this.attachments = new LinkedList<PostAttachment>();
		this.attachments.addAll(attachments);
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void setComments(List<Comment> comments) {
		this.comments = new LinkedList<Comment>();
		this.comments.addAll(comments);
	}
}
