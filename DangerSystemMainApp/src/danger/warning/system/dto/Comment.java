package danger.warning.system.dto;

import java.time.LocalDateTime;

public class Comment {
	
	private User user;
	private LocalDateTime commentedAt;
	private String text;
	private String imagePath;
	
	public Comment() {
		this.user = null;
		this.commentedAt = LocalDateTime.now();
		this.text = "";
		this.imagePath = null;
	}
	
	public Comment(User user,  LocalDateTime commentedAt, String text, String imagePath) {
		this.user = user;
		this.commentedAt =  commentedAt;
		this.text = text;
		
		this.imagePath = imagePath;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public LocalDateTime getCommentedAt() {
		return commentedAt;
	}
	
	public void setCommentedAt(LocalDateTime commentedAt) {
	
		this.commentedAt = commentedAt;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

}
