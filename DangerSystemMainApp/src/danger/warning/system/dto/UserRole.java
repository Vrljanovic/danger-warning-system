package danger.warning.system.dto;

public enum UserRole {
	ADMIN("ACTIVE"),
	CLASSIC_USER("BLOCKED");
	
	private String role;
	 
	UserRole(String role) {
        this.role = role;
    }
 
    public String getStatus() {
        return this.role;
    }

}
