package danger.warning.system.dto;

public enum PostType {

	TEXT_OR_LINK("TEXT_OR_LINK"),
	YOUTUBE_VIDEO("YOUTUBE_VIDEO"),
	IMAGE("IMAGE"),
	VIDEO("VIDEO");
	
	private String type;
	 
	PostType(String type) {
        this.type = type;
    }
 
    public String getPostType() {
        return this.type;
    }
}
