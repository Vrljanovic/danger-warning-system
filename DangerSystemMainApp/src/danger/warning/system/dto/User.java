package danger.warning.system.dto;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -1539410523934573678L;
	
	private Integer id;
	private String name;
	private String surname;
	private String username;
	private String email;
	private String passwordHash;
	private String country;
	private String avatar;
	private String region;
	private String city;
	private boolean emailNotifications;
	private boolean appNotifications;
	private int loginCount;
	private UserRole role;
	private UserStatus status;
	
	public User() {
		this.id = 0;
		this.name = "";
		this.surname = "";
		this.username = "";
		this.email = "";
		this.passwordHash = "";
		this.country = "";
		this.region = "";
		this.city = "";
		this.avatar = "";
		this.emailNotifications = false;
		this.appNotifications = false;
		this.loginCount = 0;
		this.role = UserRole.CLASSIC_USER;
		this.status = UserStatus.PENDING_APPROVAL;
	}
	
	public User(Integer id, String name, String surname, String username, String email, String passwordHash, String country, String region, String city, String avatar, boolean emailNotifications,
			boolean appNotifications, int loginCount, UserRole role, UserStatus status) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.email = email;
		this.passwordHash = passwordHash;
		this.country = country;
		this.region = region;
		this.city = city;
		this.avatar = avatar;
		this.emailNotifications = emailNotifications;
		this.appNotifications = appNotifications;
		this.loginCount = loginCount;
		this.role = role;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPasswordHash() {
		return passwordHash;
	}
	
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAvatar() {
		return avatar;
	}
	
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public boolean isEmailNotifications() {
		return emailNotifications;
	}

	public void setEmailNotifications(boolean emailNotifications) {
		this.emailNotifications = emailNotifications;
	}

	public boolean isAppNotifications() {
		return appNotifications;
	}

	public void setAppNotifications(boolean appNotifications) {
		this.appNotifications = appNotifications;
	}
	
	public int getLoginCount() {
		return this.loginCount;
	}
	
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public UserStatus getStatus() {
		return status;
	}
	
	public void setStatus(UserStatus status) {
		this.status = status;
	}
}
