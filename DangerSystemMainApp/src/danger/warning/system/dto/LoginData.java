package danger.warning.system.dto;

import java.time.LocalDateTime;

public class LoginData {
	
	private User user;
	private LocalDateTime loginTime;
	private boolean isActive;
	
	public LoginData() {
		this.user = null;
		this.loginTime = LocalDateTime.now();
		this.isActive = false;
	}
	
	public LoginData(User user, LocalDateTime loginTime, boolean isActive) {
		this.user = user;
		this.loginTime = loginTime;
		this.isActive = isActive;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public LocalDateTime getLoginTime() {
		return loginTime;
	}
	
	public void setLoginTime(LocalDateTime loginTime) {
		this.loginTime = loginTime;
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	@Override
	public String toString() {
		return user.getId() + " " + this.loginTime + " " + this.isActive;
	}
}
