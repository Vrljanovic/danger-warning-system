package danger.warning.system.dto;

public class PostAttachment {

	private String path;
	private String type;
	
	public PostAttachment() {
		this.path ="";
		this.type = "IMAGE";
	}
	
	public PostAttachment(String path, String type) {
		this.path = path;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
