package danger.warning.system.dto;

import java.util.LinkedList;
import java.util.List;

public class Warning {
	
	private Integer id;
	private String description;
	private Double longitude;
	private Double latitude;
	private boolean isUrgent;
	List<WarningCategory> categories;
	
	public Warning() {
		this.id = null;
		this.description = "";
		this.longitude = null;
		this.latitude = null;
		this.isUrgent = false;
		this.categories = new LinkedList<WarningCategory>();
	}
	
	public Warning(Integer id, String description, Double longitude, Double latitude, boolean isUrgent, List<WarningCategory> categories) {
		this.id = id;
		this.description = description;
		this.longitude = longitude;
		this.latitude = latitude;
		this.isUrgent = isUrgent;
		this.categories = new LinkedList<WarningCategory>();
		this.categories.addAll(categories);
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public boolean isUrgent() {
		return isUrgent;
	}
	
	public void setUrgent(boolean isUrgent) {
		this.isUrgent = isUrgent;
	}
	
	public List<WarningCategory> getCategories() {
		return categories;
	}
	
	public void setCategories(List<WarningCategory> categories) {
		this.categories = categories;
	}
	
	
}
