package danger.warning.system.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;

import danger.warning.system.dao.UserDAO;
import danger.warning.system.dto.User;
import danger.warning.system.dto.UserRole;
import danger.warning.system.dto.UserStatus;
import danger.warning.system.utils.StringUtils;

@ManagedBean(name = "userBean")
@RequestScoped
public class UserBean implements Serializable {
	
	private static final long serialVersionUID = -3354098130397504055L;
	
	private List<User> users;
	private User user = new User();
	private String newPassword = "";

	public UserBean() {
		users = new UserDAO().getAllUsers();
	}

	@PostConstruct
	private void init() {
		getAllUsers();
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}
	
	private void getAllUsers() {
		users = new UserDAO().getAllUsers().stream().filter(u -> u.getRole() == UserRole.CLASSIC_USER).collect(Collectors.toList());
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPassword() {
		return this.newPassword;
	}

	public String changeStatus() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("username")) {
			UserDAO userDAO = new UserDAO();
			this.user = userDAO.getUserByUniqueField("username", reqMap.get("username"));
			UserStatus newStatus = this.user.getStatus().equals(UserStatus.ACTIVE) ? UserStatus.BLOCKED : UserStatus.ACTIVE;
			this.user.setStatus(newStatus); 
			userDAO.updateUserData(this.user);
			getAllUsers();
			return "";
		}
		return null;

	}

	public String resetPassword() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("username")) {
			UserDAO userDAO = new UserDAO();
			this.user = userDAO.getUserByUniqueField("username", reqMap.get("username"));
			this.newPassword = StringUtils.getRandomString(8);
			this.user.setPasswordHash(UserDAO.getSHA256Hash(this.newPassword));
			userDAO.updateUserData(this.user);
			PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage("New Passowrd", this.newPassword));
			getAllUsers();
			return "";
		}
		return null;
	}

}
