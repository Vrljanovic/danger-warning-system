package danger.warning.system.beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import danger.warning.system.dao.CallForHelpDAO;
import danger.warning.system.dto.CallForHelp;

@ManagedBean(name = "callForHelpBean")
@SessionScoped
public class CallForHelpBean implements Serializable {

	private static final long serialVersionUID = 2464789424135484990L;
	
	private List<CallForHelp> calls = new LinkedList<CallForHelp>();
	private CallForHelp call = new CallForHelp();
	
	public CallForHelpBean() {
		calls = new CallForHelpDAO().getAllCalls();
	}
	
	public void setCalls(List<CallForHelp> calls) {
		this.calls = calls;
	}
	
	public List<CallForHelp> getCalls() {
		return this.calls;
	}
	
	public void setCall(CallForHelp call) {
		this.call = call;
	}
	
	public CallForHelp getCall() {
		return this.call;
	}

	public String blockCallForHelp() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (reqMap.containsKey("id")) {
			CallForHelpDAO callForHelpDAO = new CallForHelpDAO();
			this.call = callForHelpDAO.getCallById(Integer.parseInt(reqMap.get("id")));
			this.call.setBlocked(!this.call.isBlocked());
			callForHelpDAO.blockUnblockCall(this.call);
			calls = new CallForHelpDAO().getAllCalls();
			return "";
		}
		return null;
	}
}
