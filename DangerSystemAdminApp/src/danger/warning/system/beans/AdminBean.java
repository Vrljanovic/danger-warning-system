package danger.warning.system.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import danger.warning.system.dao.UserDAO;
import danger.warning.system.dto.User;
import danger.warning.system.dto.UserRole;

@ManagedBean (name = "adminBean")
@SessionScoped
public class AdminBean implements Serializable{
	
	private static final long serialVersionUID = -2416603297171834829L;
	
	private String username;
	private String password;
	private boolean isLoggedin;
	private boolean isWrong;
	
	public AdminBean() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLoggedin() {
		return isLoggedin;
	}

	public void setLoggedin(boolean isLoggedin) {
		this.isLoggedin = isLoggedin;
	}

	public boolean isWrong() {
		return isWrong;
	}

	public void setWrong(boolean isWrong) {
		this.isWrong = isWrong;
	}
	
	public String submit() {
		User user = new UserDAO().getUserByUniqueField("username", this.username);
		if (user != null && user.getPasswordHash().equals(UserDAO.getSHA256Hash(this.password)) && user.getRole().equals(UserRole.ADMIN)) {
			this.isLoggedin = true;
			this.isWrong = false;
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.setAttribute("user", user);
			return "index.xhtml?faces-redirect=true";
		}
		this.isLoggedin = false;
		this.isWrong = true;
		return null;
	}

	public String logout() {
        this.isLoggedin = false;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        return "login.xhtml?faces-redirect=true";
    }


}
