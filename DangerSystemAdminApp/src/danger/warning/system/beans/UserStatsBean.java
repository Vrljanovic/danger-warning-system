package danger.warning.system.beans;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.optionconfig.legend.Legend;
import org.primefaces.model.charts.optionconfig.legend.LegendLabel;
import org.primefaces.model.charts.optionconfig.title.Title;

import danger.warning.system.dao.UserDAO;

@RequestScoped
@ManagedBean(name = "userStatsBean")
public class UserStatsBean implements Serializable{

	private static final long serialVersionUID = -8518012912227850953L;
	
	private BarChartModel barModel;
	private List<String> labels = new LinkedList<String>();
	private List<Number> values = new LinkedList<Number>();
	private int totalUsers;
	private int activeUsers;
	
	
	public UserStatsBean() {
		
	}
	
	@PostConstruct
	public void init() {
		LocalDateTime dateTime = LocalDateTime.now();
		UserDAO userDAO = new UserDAO();
		for(int i = 23; i >= 0; --i) {
			labels.add(dateTime.minusHours(i).format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:00")));
			values.add(userDAO.getActiveUsersPerHour(dateTime.minusHours(i)));
		}
		totalUsers = userDAO.getTotalUsers();
		activeUsers = userDAO.getActiveUsers();
		createChart(labels, values);
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}
	
	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public List<Number> getValues() {
		return values;
	}

	public void setValues(List<Number> values) {
		this.values = values;
	}
	
	public int getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}

	public int getActiveUsers() {
		return activeUsers;
	}

	public void setActiveUsers(int activeUsers) {
		this.activeUsers = activeUsers;
	}

	private void createChart(List<String> labels, List<Number> values) {
		barModel = new BarChartModel();
        ChartData data = new ChartData();
         
        BarChartDataSet barDataSet = new BarChartDataSet();
        barDataSet.setLabel("User activity for last 24 hours");
        barDataSet.setData(values);
         
        List<String> bgColor = new ArrayList<>();
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        bgColor.add("rgb(31,126,208)");
        barDataSet.setBackgroundColor(bgColor);

         
        data.addChartDataSet(barDataSet);
        data.setLabels(labels);
        barModel.setData(data);
         
        //Options
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
         
        Title title = new Title();
        title.setDisplay(true);
        options.setTitle(title);
 
        Legend legend = new Legend();
        legend.setDisplay(true);
        legend.setPosition("top");
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        legend.setLabels(legendLabels);
        options.setLegend(legend);
 
        barModel.setOptions(options);
    }
}
