package danger.warning.system.utils;

import java.util.Random;

public class StringUtils {

	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@.,/;:";
	private static final Random random = new Random();
	
	public static String getRandomString(int length) {
		String randomString = "";
		
		for(int i = 0; i < length; ++i) 
			randomString += ALPHABET.toCharArray()[random.nextInt(ALPHABET.length())];
			
		return randomString;
	}
}
