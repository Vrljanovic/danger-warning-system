package danger.warning.system.dao;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.User;
import danger.warning.system.dto.UserRole;
import danger.warning.system.dto.UserStatus;

public class UserDAO {

	public boolean addUser(User user) {
		String sql = "insert into user values(null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getUsername());
			statement.setString(4, user.getEmail());
			statement.setString(5, user.getPasswordHash());
			statement.setString(6, user.getCountry());
			statement.setString(7, user.getRegion());
			statement.setString(8, user.getCity());
			statement.setString(9, user.getAvatar());
			statement.setBoolean(10, user.isEmailNotifications());
			statement.setBoolean(11, user.isAppNotifications());
			statement.setString(12, user.getRole().toString());
			statement.setString(13, user.getStatus().getStatus());
			
			statement.executeUpdate();
			
			ResultSet resultSet = statement.getGeneratedKeys();
			
			if(resultSet.next())
				user.setId(resultSet.getInt(1));
			resultSet.close();

		} catch (SQLException e) {
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	
	public User getUserByUniqueField(String field, String fieldValue) {
		User user = null;
		String sql = "";
		if ("username".equals(field))
			sql = "select * from user where username = ?";
		else if("email".equals(field))
			sql = "select * from user where email = ?";
		else
			sql = "select * from user where id = ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, fieldValue);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				Integer id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String username = resultSet.getString("username");
				String email = resultSet.getString("email");
				String passwordHash = resultSet.getString("password_hash");
				String country = resultSet.getString("country");
				String region = resultSet.getString("region");
				String city = resultSet.getString("city");
				String avatar = resultSet.getString("avatar");
				boolean emailNotifications = resultSet.getBoolean("email_notifications");
				boolean appNotifications = resultSet.getBoolean("app_notifications");
				String role = resultSet.getString("role");
				String status = resultSet.getString("status");
				
				user = new User(id, name, surname, username, email, passwordHash, country, region, city, avatar, emailNotifications, appNotifications, UserRole.valueOf(role), UserStatus.valueOf(status));
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return user;
	}
	
	public User checkCredentials(String username, String password) {
		User user = null;
		String passwordHash = getSHA256Hash(password);
		String sql = "select * from user where username = ? and password_hash = ?";
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, passwordHash);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				Integer id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String email = resultSet.getString("email");
				String country = resultSet.getString("country");
				String region = resultSet.getString("region");
				String city = resultSet.getString("city");
				String avatar = resultSet.getString("avatar");
				boolean emailNotifications = resultSet.getBoolean("email_notifications");
				boolean appNotifications = resultSet.getBoolean("app_notifications");
				String role = resultSet.getString("role");
				String status = resultSet.getString("status");
				
				user = new User(id, name, surname, username, email, passwordHash, country, region, city, avatar, emailNotifications, appNotifications, UserRole.valueOf(role), UserStatus.valueOf(status));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return user;
		
	}
	
	public static String getSHA256Hash(String password) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(encodedhash);
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public boolean updateUserData(User user) {
		boolean success = true;
		
		String sql = "update user set password_hash = ?, country = ?, region = ?, city = ?, avatar = ?, email_notifications = ?, app_notifications = ?, status = ? where id = ?";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, user.getPasswordHash());
			statement.setString(2, user.getCountry());
			statement.setString(3, user.getRegion());
			statement.setString(4, user.getCity());
			statement.setString(5, user.getAvatar());
			statement.setBoolean(6, user.isEmailNotifications());
			statement.setBoolean(7, user.isAppNotifications());
			statement.setString(8, user.getStatus().getStatus());
			statement.setInt(9, user.getId());
			
			statement.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
			success = false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return success;
	}
	
	public boolean isUsernameUsed(String username) {
		return getUserByUniqueField("username", username) != null;
	}
	
	public boolean isEmailUsed(String email) {
		return getUserByUniqueField("email", email) != null;
	}
	
	public List<User> getAllUsers() {
		List<User> users  = new LinkedList<User>();
		
		String sql = "select * from user";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				Integer id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String username = resultSet.getString("username");
				String email = resultSet.getString("email");
				String passwordHash = resultSet.getString("password_hash");
				String country = resultSet.getString("country");
				String region = resultSet.getString("region");
				String city = resultSet.getString("city");
				String avatar = resultSet.getString("avatar");
				boolean emailNotifications = resultSet.getBoolean("email_notifications");
				boolean appNotifications = resultSet.getBoolean("app_notifications");
				String role = resultSet.getString("role");
				String status = resultSet.getString("status");
				
				users.add(new User(id, name, surname, username, email, passwordHash, country, region, city, avatar, emailNotifications, appNotifications, UserRole.valueOf(role), UserStatus.valueOf(status)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return users;
	}
	
	public void addLoginInfo(User user) {
		String sql = "insert into login_data (user_id) values(?)";
		
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, user.getId());
			statement.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}		
	}
	
	public int getTotalUsers() {
		int total = 0;
		
		String sql = "select count(*) as total_users from user where status = 'ACTIVE'";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next())
				total = resultSet.getInt("total_users");
			resultSet.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return total;
	}
	
	public int getActiveUsers() {
		int active = 0;
		
		String sql = "select count(*) as active_users from login_data where is_active = 1";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next())
				active = resultSet.getInt("active_users");
			resultSet.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return active;
	}
	
	public int getActiveUsersPerHour(LocalDateTime dateTime) {
		int active = 0;
		
		String sql = "select count(distinct(user_id)) as number_of_users from login_data where DATE_FORMAT(login_time, '%Y-%m-%d %H:00:00') = ?";
		
		dateTime =  dateTime.withMinute(0);
		dateTime = dateTime.withSecond(0);
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00:00")));
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
				active = resultSet.getInt("number_of_users");
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return active;
	}
}
