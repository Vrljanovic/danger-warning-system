package danger.warning.system.dao;

import java.net.URI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import danger.warning.system.db.connection.ConnectionPool;
import danger.warning.system.dto.CallForHelp;
import danger.warning.system.dto.Category;

public class CallForHelpDAO {

	
	public boolean addCallForHelp(CallForHelp callForHelp) {
		String sql = "insert into call_for_help values(null, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, callForHelp.getTitle());
			statement.setString(2, callForHelp.getLocation());
			statement.setString(3, callForHelp.getDateTime().toString());
			statement.setString(4, callForHelp.getDescription());
			statement.setString(5, callForHelp.getImageUri().toString());
			statement.setString(6, callForHelp.getCategory().getCategory());
			statement.setBoolean(7, callForHelp.isReported());
			statement.setBoolean(8, callForHelp.isBlocked());
			
			statement.executeUpdate();
			
			ResultSet resultSet = statement.getGeneratedKeys();
			
			if(resultSet.next())
				callForHelp.setId(resultSet.getInt(1));
			resultSet.close();

		} catch (SQLException e) {
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return true;
	}
	
	public List<CallForHelp> getAllCalls() {
		String sql = "select * from call_for_help";
		
		List<CallForHelp> calls = new LinkedList<CallForHelp>();
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement  = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				int id = resultSet.getInt("id");
				String title = resultSet.getString("title");
				String location =resultSet.getString("location");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime dateTime = LocalDateTime.parse(resultSet.getString("date_time"), formatter);
				String description = resultSet.getString("description");
				URI imageUri = URI.create(resultSet.getString("image_uri"));
				Category category = Category.valueOf(resultSet.getString("category"));
				boolean isReported = resultSet.getBoolean("is_reported");
				boolean isBlocked = resultSet.getBoolean("is_blocked");
				calls.add(new CallForHelp(id, title, location, dateTime, description, imageUri, category, isReported, isBlocked));
			}
		} catch (SQLException ex) {
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return calls;
	}
	
	public CallForHelp getCallById(int id) {
		String sql = "select * from call_for_help where id = ?";
		
		CallForHelp call = null;
		
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement  = connection.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				String title = resultSet.getString("title");
				String location =resultSet.getString("location");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime dateTime = LocalDateTime.parse(resultSet.getString("date_time"), formatter);
				String description = resultSet.getString("description");
				URI imageUri = URI.create(resultSet.getString("image_uri"));
				Category category = Category.valueOf(resultSet.getString("category"));
				boolean isReported = resultSet.getBoolean("is_reported");
				boolean isBlocked = resultSet.getBoolean("is_blocked");
				
				call = new CallForHelp(id, title, location, dateTime, description, imageUri, category, isReported, isBlocked);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		return call;
	}
	
	public boolean blockUnblockCall(CallForHelp call) {
		String sql = "update call_for_help set is_blocked = ? where id = ?";
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setBoolean(1,  call.isBlocked());
			statement.setInt(2, call.getId());
			
			statement.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return true;
	}
	
	public boolean reportCall(CallForHelp call) {
		String sql = "update call_for_help set is_reported = ? where id = ?";
		Connection connection = null;
		
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setBoolean(1,  call.isReported());
			statement.setInt(2, call.getId());
			
			statement.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return true;
	}
}
