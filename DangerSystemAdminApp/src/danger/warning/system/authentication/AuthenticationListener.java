package danger.warning.system.authentication;

import java.io.IOException;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthenticationListener implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws ServletException, IOException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession(false);
		
		String loginURL = request.getContextPath() + "/login.xhtml";
		String indexURL = request.getContextPath() + "/index.xhtml";
		
		boolean loggedIn = (session != null) && (session.getAttribute("user") != null);
		boolean loginRequest = request.getRequestURI().equals(loginURL);
		boolean indexRequest = request.getRequestURI().contentEquals(indexURL);
		boolean resourceRequest = request.getRequestURI().startsWith(request.getContextPath() + "/resources/")
				|| request.getRequestURI().startsWith(request.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER + "/");
		if (resourceRequest)
			chain.doFilter(request, response);
		else if(loginRequest && loggedIn)
			response.sendRedirect(indexURL);
		else if (loginRequest)
			chain.doFilter(request, response);
		else if (loggedIn) {
			if(indexRequest || resourceRequest)
				chain.doFilter(request, response);
			else
				response.sendRedirect(indexURL);
		}
		else {
			response.sendRedirect(loginURL);
		}
	}

	@Override
	public void destroy() {

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
