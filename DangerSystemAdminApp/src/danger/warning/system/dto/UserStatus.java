package danger.warning.system.dto;

public enum UserStatus {
	ACTIVE("ACTIVE"),
	BLOCKED("BLOCKED"),
	PENDING_APPROVAL("PENDING_APPROVAL");
	
	private String status;
	 
    UserStatus(String status) {
        this.status = status;
    }
 
    public String getStatus() {
        return this.status;
    }

}
