package danger.warning.system.dto;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDateTime;

public class CallForHelp implements Serializable{
	
	private static final long serialVersionUID = -2047422834375894683L;
	
	private Integer id;
	private String title;
	private String location;
	private LocalDateTime dateTime;
	private String description;
	private URI imageUri;
	private Category category;
	private boolean isReported;
	private boolean isBlocked;
	
	public CallForHelp () {
		this.id = null;
		this.title = "";
		this.location = "Unknown";
		this.dateTime = LocalDateTime.now();
		this.description = "";
		this.imageUri = null;
		this.category = Category.OTHER;
		this.isReported = false;
		this.isBlocked = false;
	}
	
	public CallForHelp (Integer id, String title, String location, LocalDateTime dateTime, String description, URI imageUri, Category category, boolean isReported, boolean isBlocked) {
		this.id = id;
		this.title = title;
		this.location = location;
		this.dateTime = dateTime;
		this.description = description;	
		this.imageUri = imageUri;
		this.category = category;
		this.isReported = isReported;
		this.isBlocked = isBlocked;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public URI getImageUri() {
		return imageUri;
	}

	public void setImageUri(URI imageUri) {
		this.imageUri = imageUri;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public boolean isReported() {
		return isReported;
	}
	
	// Iako je glupo, drugacije ga JSF nece traziti!!!!
	// Izvinder zaovo
	public String getIsReported() {
		return isReported ? "YES" : "NO";
	}
	
	// Iako je glupo, drugacije ga JSF nece traziti!!!!
	// Izvinder zaovo
	public String getIsBlocked() {
		return isBlocked ? "YES" : "NO";
	}

	public void setReported(boolean isReported) {
		this.isReported = isReported;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	
	
}
